---
layout: mission
title: 'Zone 3'
---

# Kontinent 3 (Gebirge mit Binnensee)

![Zonenaufbau von Kontinent 3. Es dominiert Dschungel im Westen und Süden. Durch die Mitte zieht sich ein 
Gebirge. Im Osten gibt es Waldgebiete. Vereinzelte Aqua/Insel Zonen
befinden Sich im Nordwesten, und an der Ostküste.](../images/K3_Zonen.png)

## Bildnachweise

* [Aqua by Ady Arif Fauza](https://pixabay.com/de/users/Ady_Fauzan-6127768/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2614341)
* [Binnensee by David Mark](https://pixabay.com/de/users/12019-12019/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1679708)
* [Dschungel by bere von awstburg](https://pixabay.com/de/users/bere69-3506936/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1865639)
* [Gebirge by Pexels](https://pixabay.com/de/users/Pexels-2286921/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1837129)
* [Wald by Valiphotos](https://pixabay.com/de/users/Valiphotos-1720744/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1072828)

## Downloads 

* [Regionskarten Aqua](../images/Regionskarten_Aqua.pdf)
* [Regionskarten Binnensee](../images/Regionskarten_Binnensee.pdf)
* [Regionskarten Dschungel](../images/Regionskarten_Dschungel.pdf)
* [Regionskarten Gebirge](../images/Regionskarten_Gebirge.pdf)
* [Regionskarten Wald](../images/Regionskarten_Wald.pdf)
* [Zonenreferenzblatt für die Justifiers](../images/K3_Zonenreferenzblatt_Justifiers.pdf)
* [Zonenreferenzblatt für den Erzähler](../images/K3_Zonenreferenzblatt_Erzaehler.pdf)

## Was hier passiert

Die Justifiers treffen auf dem Kontinent ein und landen an der westlichen Küste im Dschungel. Von da aus 
müssen sie sich durch den Kontinent kämpfen. 

Neben den Gefahren, die sie dabei in den einzelnen Regionen überstehen müssen, begegnen die Abenteurer einem 
Justifiers Team eines anderen Konzerns. Außerdem treffen Sie auf weitere Bewohner des Planeten. 

Auf ihrer Reise erfahren die Justifiers Neues über die Geschichte des Planeten und wie es zu dem fehlenden 
Stück Berg kommt, dass die sie vielleicht schon während der Orbitalexploration entdeckt hatten.

## Szene 1: Dschungelcamp

<div class="readaloud">
Ihr fliegt auf den Kontinent zu und erreicht die Westküste, welche unweit des Ufers dicht bewachsen ist.

Die Scanner erfassen viele organische Strukturen und Lebenwesen in allen Formen und Größen. Aber ihr
glaubt im Rauschen noch ein weiteres Signal auszumachen. Aber es ist viel zu schwach und ihr könnt ohne 
präzisere Einstellungen das Signal nicht isolieren. 
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + TECHNIK [ELEKTRONIK] (3)</span>
  </div>
  <div class="success">
    <span>Erfolg</span>
    <p class="readaloud">Das ist definitv ein künstliches Signal ... und es ist verschlüsselt. Also setzt ihr die Bordcomputer
	  darauf an. Es dauert eine gefühlte Ewigkeit, bis der Rechner etwas ausspuckt. Im wesentlichen teilt
		die Ausgabe Euch aber nur mit, dass die Verschlüsselung zu stark ist.</p>

		<p class="readaloud">Aber das System ist mit seinen Berechungen noch nicht fertig und durchsucht die Datenbank nach 
		vergleichbaren Signaturen und Verschlüsselungen. Es endet mit der Ausgabe von zwei Buchstaben: UI</p>
		
		<p>Lesen Sie den Justifiers den Text aus <a href="#united-industries">United Industries</a> vor. Und fahren sie 
		anschließend hier fort.</p>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <p class="readaloud">Ihr seid Euch sicher, dass es ein Signal ist, was nicht auf diese Welt gehört. Irgendwie kommt es Euch sogar sehr 
		vertraut vor. Aber plötzlich ist es verschwunden. </p>
  </div>
</div>

<div class="readaloud">
Plötzlich kommt Euch ein Gedanke: Wenn ihr das Signal auffangen konntet, dann geht das vielleicht auch anders herum. Das heißt, ihr solltet
zusehen, dass ihr das Shuttle hier irgendwo runter bringt. Der Dschungel wird euch dabei helfen das Shuttle zu verstecken. 
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Für den Pilot:</span>
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
  </div>
  <div class="success">
    <span>Erfolg</span>
    <span class="readaloud">Ihr findet eine Lücke im Dschungel, die ungefähr so groß ist wie das Shuttle. Beim Anflug kracht und knackt es und
		die Bäume und Büsche hinterlassen einige Spuren am Shuttle. Aber nichts problematisches.</span>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <span class="readaloud">Nach Eurer "Landung", ist ein shuttle-großes Loch im Dschungel. Euer wertvollstes Gerät und einzige Chance von diesem Planeten
		herunterzukommen, hat etliche Kratzer abbekommen. Aber zum Glück nichts, was der Schrauber nicht wieder hinbekommt.</span>
		<p>Das Shuttle gilt als <span class="emphasize">beschädigt</span> und muss repariert werden.</p>
  </div>
</div>

<div class="readaloud">
Das Shuttle ist im Dschungel gut versteckt und ihr fahrt die Systeme auf ein Mininum herunter bevor ihr Euch hier häuslich einrichtet. Dieses Dschungelcamp
wird wohl erstmal so etwas wie Euer Zuhause sein für die nächste Zeit. 
</div>

## Exploration

Die Justifiers haben jetzt ihr Camp errichtet und müssen zuerst das Shuttle reparieren, bevor sie mit der Exploration weitermachen können. 
Direkt nach der Landung sollte natürlich zuerst <a href="#dschungel-iii">Dschungel III</a> erforscht werden. Anschließend kann nach den normalen
Explorationsregeln fortgefahren werden.


### Aqua I

#### Regionsbeschreibung

<div class="readaloud">
Ihr erreicht einen schmalen Strand und lasst euren Blick über das Meer schweifen. In nicht allzugroßer Entfernung erkennt ihr eine Gruppe von Inseln, 
deren Zahl ihr von dieser Position aus nicht abschätzen könnt. Die Größe der aus dem Meer ragenden Eilande ist genauso verschieden, wie die Vegetation 
die darauf vorherrscht. Es reicht von dicht bewachsenenen, dschungelartigen Wäldern bis hin zu kargen, schwarzen Felsen, auf denen gar nichts zu wachsen 
scheint. 
</div> 

#### Regionsereignis (Permanent bis einmal erfolgreich)

Wenn die Justifiers mit einem <span class="emphasize">Luftfahrzeug</span> anreisen, können Sie die Inseln damit einfach erreichen, sobald sie das 
Regionsereignis einmal erfolgreich beendet haben. Mit <span class="emphasize">Bodenfahrzeugen</span> können Sie den Strand erreichen und 
können so auch das Ereignis auslösen. Anschließend müssen sie aber entweder zurück zum Shuttle um ein Luftfahrzeug zu holen. Haben sie keines mitgenommen
oder sind alle beschädigt, können Sie sie Boote bauen lassen. Nutzen sie dafür eine Sammelherausforderung auf KÖRPER + SURVIVAL (10).

Mit <span class="emphasize">Wasserfahrzeugen</span> ist die Region nicht zu erreichen, da zahlreiche Untiefen um die Inseln herum eine Annährung von der Seeseite unmöglich macht. 

Die Justifiers treffen nun auf einen Teil des United Industries Teams, welches ohne Umschweife direkt das Feuer auf sie eröffnen wird. 
Das Gefährt des Gegeners und die Rüstung tragen keinerlei Abzeichen oder Kennzeichnungen, welche die Justifiers auf die Herkunft des Teams schließen 
lassen.

##### Überraschung 
Lesen Sie beim ersten Eintreffen der Justifiers folgenden Text vor und stellen Sie sie vor die anschließende Herausforderung. Ist das Ereignis nicht 
beim ersten Eintreffen erfolgreich und die Justifiers kommen wieder, können Sie direkt mit dem Kampf beginnen, da sie sich nicht nochmal 
überrumplen lassen.

<div class="readaloud">
<p>
Kurz nach eurem Eintreffen am Strand, taucht aus dem Meer plötzlich etwas auf, das zuerst aussieht wie ein riesiger Rochen in dessen Mitte 
eine Art Glaskuppel sitzt. Als ihr genauer hinschaut, seht ihr wie sich die Kuppel öffnet und heraus schaut der Kopf eines Bisons auf dessen Schulter 
ein langes dickes Rohr liegt, dass direkt auf Euch gerichtet ist. 
</p>
</div>

<div class="challenge-box">
  <div class="challenge">
		<span class="role">Die Justifiers müssen dem Granatwerfer-Angriff ausweichen und sich in Deckung bringen.</span> 
    <span class="dice">Herausforderung auf GEIST + REFLEXE (2)</span>
  </div>
  <div class="success">
    <span>Erfolg</span>
		Alle Justifiers, die die Herausforderung bestehen können sich rechtzeitig in Deckung bringen und direkt am Kampf teilnehmen.
  </div>
  <div class="failure">
    <span>Misserfolg</span> 
		<span>Lesen Sie für alle Justifiers, die die Herausforderungen nicht bestanden haben, folgenden Text vor</span>
		<p class="readaloud">Ihr versucht Euch in Sicherheit zu bringen, als das Projektil in Eurer unmittelbaren Nähe einschlägt. 
		Ihr werdet von der Druckwelle erfasst und mehrere Meter über den Strand geschleudert. In euren Ohren fiepst es bedenklich und ihr braucht
		einen Moment um Euch zu sammeln und zu prüfen ob alle Eure Gliedmaßen noch da sind, wo sie vor ein paar Sekunden zuvor waren.</p>	
		<span>Alle getroffenen Justifiers verlieren </span><span class="emphasize">-1 Ausdauerpunkt</span><span> und können erst nach einer Runde 
		am Kampf teilnehmen</span>
  </div>
</div>

##### Kampf

Die Justifiers müssen nun versuchen das Gefährt außer Gefecht zu setzen um die Region weiter erkunden zu können.

<div class="enemy">
	<div class="header">Subray</div>
	<ul>
		<li><span>Angriff</span>3/0/0</li>
		<li><span>Schaden</span>2</li>
		<li><span>Verteidigung</span>3</li>
		<li><span>Ausdauer</span>12</li>
		<li><span>Gefahr</span>2</li>
	</ul>
	<div>
		<span class="emphasize">Manöver</span>
		<span>Das Uboot verfügt über zwei Bordkanonen kann also in jeder Runde zweimal angreifen. Dabei greift es zuerst die Justifiers in der Nähe an, 
		also die, welche vom letzten Granaten-Angriff nicht getroffen wurden. Anderfalls werden beliebige Justifiers angegriffen. 
		</span>
	</div>
	<div class="talent">
		<span class="header">Granatwerfer</span>
		<span>
		In jeder dritten Runde feuert der Bison-Beta aus der Kuppel heraus mit einem Granatwerfer auf die Justifiers. Dabei müssen alle Justifiers
		die Herausforderung für den Granatwerfer-Angriff ausführen.
		</span>
	</div>
</div>

Sobald die Justifiers den Subray besiegt haben, lesen Sie folgenden Text vor:

<div class="readaloud">
Verdammter Mist, egal was auch immer das für ein Gefährt war. Da drin saß ein Beta und das heißt, es ist definitv ein anderes aktives Justifiers Team
auf diesem Planeten und versucht Euch euren wohlverdienten Bonus streitig zu machen. Und ganz offensichtlich sind die nicht auf Teilen aus. Ihr müsst 
definitiv etwas dagegen tun. Wenn das Nachschub Team hier eintrifft und ein anderer Konzern auch schon hier ist, gibt das eine ganze Menge Ärger. 
</div>

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (5)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
		<span class="dice">Bonus Luftfahrzeug +2</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud">
Das Zusammentreffen mit dem gegnerischen Justifiers Team hat die Lage komplett geändert und ihr seid wesentlich vorsichtiger als bisher, als ihr 
beginnt, die Region genauer unter die Lupe zu nehmen und die Scanner anzuwerfen. 
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Sammelherausforderung auf GEIST + COMPUTER</span>
  </div>
	<div class="collect-challenge-result">
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>1</span>
    	<span class="readaloud">
			Die Scanner liefern euch eine grobe Karte der Inseln und markieren die Stelle an der ihr das Shuttle des andereren Justifiers Teams vermutet.
			</span>
			<p>Markieren Sie den Justifiers das <span class="emphasize">Erkundungsziel II</span>. Die Justifiers können nun ab der nächsten 
			Explorationrunde zu diesem Ziel reisen und das Abenteuer <a href="#szene-2-clash-of-corporations">Szene 2: Clash of Corporations</a> spielen.</p>
  	</div>
		<div>
    	<span>2</span>
    	<span class="readaloud">
			Das Riff, welches die Inseln umgibt ist sehr nährstoffhaltig, weshalb sich dort Unmengen an Fischen und anderen Meereslebewesen sammeln. 
			</span>
		</div>
		<div>
    	<span>3+</span>
    	<span class="readaloud">
			Im Inneren des Riffs gibt es mehrere hydrothermale Quellen zu geben, was auf eine enorme vulkanische Aktivität innerhalb des Ringes schließen 
			lässt. Vielleicht kann man das nutzen für die Energiegewinnung auf dem Planeten. 
			</span>
			<span class="emphasize">+1 MP</span>
		</div>
	</div>
</div>

### Aqua II
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Aqua III
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Binnensee I
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Binnensee II

#### Regionsbeschreibung

<div class="readaloud">
Das Zielgebiet besteht zum größten Teil aus einem Süßwassersee. Das dunkle Wasser deutet auf eine extreme Tiefe hin. Der See endet scheinbar an einer Gebirgswand, 
welches von hier aussieht als hätte sie jemand in der Mitte mit einem Messer zerschnitten. 
</div> 

#### Regionsereignis

<div class="readaloud">
Ihr beginnt Euch umzusehen und wollt erste Proben nehmen, da springt Euch ein <a href="#ui-justifier">Tiger Beta</a> an und geht direkt zum Angriff über.
</div>

Die Justifiers wissen an dieser Stellte ggf. noch nicht dass es sich bei den Gegnern um Justifiers von United Industries handelt. Die Gegner tragen keinerlei
Erkennungszeichen. Lediglich die Waffen lassen sich ggf. als UI Waffen identifizieren, aber die nutzen auch viele andere Konzerne.
Ist mehr als ein Justifier für die Exploration hierhergekommen, beginnt ein weiterer <a href="#ui-justifier">UI Justifier</a> auf die Gruppe zu schießen. 

Die Regionserkundung kann erst begonnen werden, wenn alle UIs in dieser Region besiegt sind.

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud">
Nach dem Kampf braucht ihr eine kurze Verschnaufpause, ihr startet also schnell die Scanner und während die suchen, lehnt ihr Euch zurück und lasst 
Euren Blick über den See schweifen.
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + WISSEN [BIOLOGIE] (2)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<p class="readaloud">
		Nach einiger Zeit kommt ihr zur Ruhe und auch Eure Umgebung scheint sich von dem Schock und der Lautstärke des Kampfes erholt zu haben.
		Am Ufer, westlich Eurer Position, sammelt sich eine Herde von Tieren um zu trinken. Auf der anderen Seite seht ihr Fische, die aus dem Wasser springen
		um Insekten zu fangen. Auch die Vegetation um den See herum scheint vielfältig sein.
		</p>
		<p class="readaloud">
		Ihr könnt erkennen, dass der See das zentrale Element eines breitgefächerten Ökosystems ist. Er ist die Grundlage für die Vegetation, welche wiederum 
		den Tieren Nahrung und Schutz bietet. 
		</p>
		<p>Die Justifiers erhalten <span class="emphasize">+2 MP</span></p>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		Das ist ja alles ganz schön hier, aber die Mücken die hier in der Nähe des Sees herumschwirren machen Euch ganz schön zu schaffen und ihr seht zu, dass ihr 
		wieder zurück in Euer Fahrzeug kommt.
		</span>
 	</div>
</div>

<div class="readaloud">
Als ihr wieder im Fahrzeug seid, kommen die ersten Meldungen der Scanner herein. Eine Meldung nach der anderen poppt auf den Bildschirmen auf und melden
den Fund seltener Erden und verschiedener Edelmetalle. Alle auf dem Grund des Sees der unfassbar tief zu sein scheint, was den Abbau der Rohstoffe wesentlich 
erschweren wird.
</div>
<p>
Für diese spannende Entdeckung erhalten die Justifiers <span class="emphasize">+2 MP</span>.
</p>

### Dschungel I
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Dschungel II
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Dschungel III

#### Regionsbeschreibung

<div class="readaloud">
<p>Der Rand des Dschungels, in dem ihr das Shuttle versteckt habt, grenzt an einen schmalen Sandstrand an dem die Wellen branden. Dieser Kontinent 
hätte vielleicht Potential für die Tourismusbranche als Urlaubswelt, wenn die Reise nicht Monate mittels TransMatt Portal dauern oder 
Unsummen für einen Überlicht-Sprung verschlingen würde. Es ist also ein exklusives Reiseziel.</p>
</div> 

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
		<span class="dice">Luftfahrzeug Malus -2</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud">
Ihr schmeißt die Scanner an und macht zusätzlich einen langen Spaziergang im Strand während die Sonnen untergehen.
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf SEELE + WAHRNEHMUNG</span>
  </div>
  <div class="collect-challenge-result">
    <div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
    <div>
      <span>1</span>
      <span class="readaloud">
				Es wird kein neues AtLantis, aber es ließe sich defintiv was daraus machen. Der Sonnenuntergang ist 
				wirklich atemberaubend, wenn man auf so was steht. Was natürlich auf keinen von Euch harten Typen 
				zutrifft. 
      </span>
    </div>
    <div>
      <span>2+</span>
      <span class="readaloud">
				Die Pflanzen am Rand des Dschungels, riechen nach Ananas und Kokusnuss und die Scanner erfassen 
				einen natürlichen Alkoholgehalt. Ein Strand mit Pina-Colada Pflanzen, das ist mal definitiv ein 
				Alleinstellungsmerkmal. Und das wird zur Abwechslung mal ein entspannter Abend im Shuttle.
      </span>
			<span class="emphasize">+1 MP</span>
    </div>
	</div>
</div>

### Dschungel IV
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Dschungel V
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Dschungel VI
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>
### Dschungel VII
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Dschungel VIII
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Gebirge I
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Gebirge II
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Gebirge III

#### Regionsbeschreibung

<div class="readaloud">
Der wesentliche Teil der Region besteht aus einem Teil des Gebirges, das sich durch den Kontinent zieht. An den Fuß des Berges grenzt jedoch ein schmales Stück dichten Waldes,
welcher das Felsmassiv von einem riesigen See trennt. Ein Seitenarm des Sees fließt durch eine tiefe Kluft, welche das Gebirge hier trennt.  
</div> 

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
		<span class="dice">Bodenfahrzeugmalus -3</span>
  </div>
</div>

#### Regionsscan

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf SEELE + WAHRNEHMUNG (3)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<p class="readaloud">
		Als ihr das Waldgebiet genauer untersucht, fällt Euch auf,  dass dieses an einigen Stellen deutlich weniger dicht bewachsen zu ist. Es scheinen fast gerade Linien zu sein, die
		zum See und zu verschiedenen anderen Waldgebieten führen. Aber alle diese Pfade laufen auf einen Punkt am Fuß des Berges zu. 
		</p>
		<p class="readaloud">
		An dem Punkt, an dem alle Pfade zusammenlaufen, gibt es eine Art Siedlung von Baumhäusern, welche durch Hängebrücken miteinander verbunden sind.
		</p>
		<p class="readaloud">
		Ihr könnt zwar keine Bewohner des Dorfes erkennen, aber die Siedlung ist definitiv nicht unbewohnt. Alle Türen und Fenster sind geschlossen aber hier und da gibt es Fackeln, 
		die immernoch ein wenig dampfen, nach dem sie scheinbar in Eile gelöscht wurden. Ihr solltet vielleicht nochmal wiederkommen und Euch ein weniger unfälliger verhalten.
		</p>
		<p>Decken sie den Justifiers das <span class="emphasize">Erkundungsziel I</span> auf. Die Justifiers können das Dorf nur betreten, wenn alle Gruppenmitglieder dabei
		sind. In jedem Fall können die Justifiers erst in der nächsten Explorationsrunde in das Dorf gehen und das Abenteuer <a href="#szene-3-friede-sei-mit-dir">Friede sei mit dir</a>
		spielen</p>  
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		Das Gebirge ist karg und langweilig und das Waldstück ist so dicht, dass sich hier nur schwer etwas finden lässt. Nach einiger Zeit dreht ihr frustriert um,
		mit dem sicheren Gefühl hier irgendwas übersehen zu haben.
		</span>
 	</div>
</div>

### Gebirge IV
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Wald I
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Wald II
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Wald III
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

### Wald IV
<div class="todo">TBD</div>
#### Regionsbeschreibung

<div class="readaloud">
</div> 

#### Regionsereignis

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud"></div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf ATTRIBUT + FERTIGEIT (Erfolgsgrad)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		</span>
 	</div>
</div>

## United Industries

<div class="readaloud">
<p>Die Gründung von United Industries liegt kurz vor dem 2. Konzern-Krieg, welchen der Zusammenschluss aus Vickers, Heckler&Koch und Steyr 
für einen rasanten Aufstieg nutzte und heute zu einem der Mega-Kons zählt. Auf dem Weg dorthin schluckte UI diverse andere Firmen und 
Konzerne, viele davon durch aggressive Verhandlungen und feindliche Übernahmen. Darunter befanden sich teilweise namenhafte Rüstungsschmieden
wie Colt und Smith&Wesson. Deshalb ist United Industries heute einer der der führenden Unternehmen im Waffenbau. </p>

<p>Seit dem gescheiterten Versuch AriesOne, einen weiteren großen Rüstungskonzern, per Waffengewalt zu übernehmen, liegen die beiden Konzerne in 
einem erbitterten Wettstreit. Wären die beiden Kontrahenten nicht vollkommen unabhängig sein vom Einfluss der Länderregierungen, würde 
zwischen ihnen vermutlich ein handfester Krieg ausbrechen. </p>

<p>Während auch andere Konzerne, vor allem Megas, mit harten Bandagen kämpfen, hat UI als Rüstungskonzern Zugriff auf fortschrittlichste 
Waffentechnik aus dem eigenen Hause und somit einen enormen Vorteil im Kampf. Aber United Industries kann auch unter dem Radar fliegen und
beschäftigt, Gerüchten zufolge, eine der bestausgebildsten Spionage- und Geheimdienstabteilungen der Galaxie. </p>
</div>

## Szene 2: Clash of Corporations

### Was hier passiert

Die Justifiers finden das Shuttle des anderen Teams von UI und müssen dieses bekämpfen um ihre eigene Mission zu erfüllen und dem Nachschub-Team 
einen sicheren Planeten vorweisen zu können, wenn dieses ankommt. 

Das ist aber leichter gesagt als getan. Das Shuttle ist zwar offensichtlich abgestürzt aber das UI Team hat etliche Sicherheitsmaßnahme getroffen 
um es zu sichern. Außerdem ist das UI Team wesentlich besser bewaffnet und hat mehr militärisch ausgebildete Mitglieder als die Gruppe. Ein direkter 
Angriff ist also ausgeschlossen und die Spieler müssen einige Guerilla Taktiken wie Sabotage und Überraschungsangriffe anwenden um erfolgreich zu sein.

### Das Shuttle

<div class="readaloud">
<p>
Auf eurem Weg zum Shuttle des anderen Justifier-Teams müsst ihr Euch durch einen dichten Dschungel kämpfen. Die offensichtlichen Pfade durch diese
grüne Hölle meidet ihr, da ihr vermutet, dass diese von euren Gegnern angelegt wurden. Denen wollt ihr aber erstmal nicht direkt begegenen, solange
ihr nicht mehr über sie wisst.
</p>
<p>
Ihr kommt entsprechend nur sehr langsam voran aber schlussendlich könnt ihr in einiger Entfernung eine Lichtung erkennen, die von einer Vielzahl 
abgebrochener und ausgerissener Bäume gesäumt ist. Eine lange Schleifspur am Boden sagt Euch, dass die Landung des anderen Teams offensichtlich
nicht viel besser war, als eure Eigene. Aber sie zeigt Euch in welche Richtung ihr Euch bewegen müsst. 
</p>
</div>


Mittels der nachfolgenden Herausforderungen können die Spieler die Anzahl der gegnerischen Justifiers im finalen Kampf reduzieren. Grundsätzlich stehen
ihnen sonst während des finalen Kampfes <span class="emphasize">Anzahl Justifiers + 5</span> Feinde gegenüber.

An der folgenden Sammelstufenherausforderung müssen nicht alle Justifiers in jeder Runde teilnehmen. Im Falle einer Teilnahme muss aber zusätzlich eine 
Herausforderung auf <span class="emphasize">GEIST + HEIMLICHKEIT (2)</span> bestanden werden. Andernfalls muss der Justifiers einen Erfolg von seiner
Sammelherausforderung in dieser Runde abziehen.


<div class="challenge-box">
  <div class="challenge">
		<span class="role">Sammelstufenherausforderung für 5 Runden</span>
    <span class="dice">KÖRPER + SURVIVAL [DSCHUNGEL]</span>
		<div>
		<span class="emphasize">J</span> steht in der nachfolgenden Tabelle für <span class="emphasize">Anzahl der Justifiers (gesamt)</span>, also nicht nur die, die an der 
		Herausforderung teilnehmen
		</div>
  </div>
	<div class="collect-challenge-result">
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>1 * J</span>
    	<span class="readaloud">
				Ihr beginnt damit die Umgebung des Shuttle-Wracks auszukundschaften und umkreist das Gebiet weiträumig. Plötzlich stoppt Euer Vordermann und 
				gibt Euch per Handzeichen zu erkennen Euch auf keinen Fall weiter zu bewegen und die Füße still zu halten: Vor Euch befindet sich eine 
				Tretmine. Und es scheint nicht die einzige im Umkreis zu sein. 	
			</span>
			<p>Die Justifiers könnten jetzt auf die Idee kommen die Tretminen zu deaktivieren, auszugraben und an anderen Stellen wieder zu vergraben.
			Fahren sie in dem Fall kurzfristig mit <a href="#tretminen">Tretminen</a> fort, bevor sie diese Herausforderung weiter würfeln lassen.
			</p>
  	</div>
		<div>
    	<span>2 * J</span>
    	<span class="readaloud">
			Ihr habt die Absturzstelle einmal großzügig umkreist und beginnt Euch nun langsam in Richtung des Shuttles zu bewegen. Doch vernehmt ihr plötzlich
			Stimmen in Eurer Nähe. Ihr bleibt stehen und prüft die Richtung aus der diese zu kommen scheinen, als ihr festestellt, dass die Stimmen näher 
			kommen. 
			</span>
			<p class="readaloud">
			Durch die dichten Blätter erkennt ihr den riesigen Kopf eines Bisons, das in das JUST an seinem Arm spricht. Der Beta bewegt sich auf 
			einem schmalen Pfad unweit Eurer aktuellen Position in Richtung des Shuttles.
			Das ist eindeutig Euer Freund aus dem U-Boot am Strand, der sich aufgrund seiner Verletzungen nur langsam vorwärtsbewegen kann. 
			</p>
			<div class="challenge-box">
			  <div class="challenge">
					<span class="role">Überraschungsangriff</span>	
			    <span class="dice">Nahkampfangriff KÖRPER + NAHKAMPF (3)</span>
			  </div>
			  <div class="success">
			    <span>Erfolg</span>
				    <span class="readaloud">
						Der Bison-Beta weiß nicht wie ihm geschieht, als ihr aus dem Duschungel brecht und ihn im Handumdrehen auschaltet. Er trägt immer noch 
						den <a href="#granatwerfer">Granatwerfer</a> bei sich, mit dem er am Strand auf Euch geschossen hat. Aber die Munition scheint mit dem 
						U-Boot untergegangen zu sein. 
						</span>
						<span class="emphasize">Anzahl der Gegner im finalen Kampf reduziert sich um 1</span>
			  </div>
			  <div class="failure">
			    <span>Misserfolg</span>
				    <span class="readaloud">
						Der Bison-Beta bemerkt Euch, bevor ihr ihn erreichen könnt und rennt direkt in Richtung des Shuttles, ohne sich noch einmal umzudrehen. 
						Die Verfolgung ist viel zu riskant und ihr seid offensichtlich endteckt worden. Ihr müsst Euch zurückziehen und später wiederkommen.
						</span>
						<span class="readaloud">Aber Glück im Unglück: Bei seiner überstürzten Flucht, hat er den <a href="#granatwerfer">Granatwerfer</a> verloren, 
						mit dem er am Strand 	auf Euch geschossen hat.	Nur war er nicht so freundlich, auch noch die Munition hier zu lassen.</span>
						<span class="emphasize">Die Justifiers verlieren eine Missionsrunde</span>
			  </div>
			</div>
			<p>
			Die Justifiers erhalten nach der Herausforderung (unabhängig ob erfolgreich oder nicht) das Fundstück <a href="#granatwerfer">United Industries 
			Granatwerfer</a>, verfügen aber nicht über die notwendige Munition um diesen einzusetzen. 
			</p>
		</div>
		<div>
    	<span>3 * J</span>
    	<span class="readaloud">
			Ihr erreicht den Rand der Lichtung, die durch den Absturz des Shuttles entstanden ist und die dem UI Team mittlerweile als Basis zu dienen
			scheint. Am Rande der Lichtung sind verschiedene Kisten als eine Art Barrikade aufgetürmt. 
			</span>
			<div class="challenge-box">
			  <div class="challenge">
					<span class="role">Untersuchen der Kisten</span>	
			    <span class="dice">SEELE + WAHRNEHMUNG (2)</span>
			  </div>
			  <div class="success">
			    <span>Erfolg</span>
				    <span class="readaloud">
						Als ihr die Kisten durchsucht, findet ihr einen Granatwerfer, der dem des Bison-Betas ähnlich sieht, bis auf die Tatsache, das er 
						völlig demoliert ist. Aber unter dem ganzen Schrott liegt eine <a href="#granatwerfer">Granate</a>, die sehr danach aussieht, als würde
						sie in den gefundenen Granatwerfer passen.
						</span>
						<p>
						Die Justifiers können nun mit dem Granatwerfer auf das Shuttle schießen <span class="emphasize">KÖRPER + FERNKAMPF (3)</span>. 
						Im Erfolgsfall erledigen so einen weiteren UI Justifier. Falls der Bison Beta zuvor entkommen ist, schaffen Sie es sogar zwei Gegner 
						auszuschalten.
						</p>
			  </div>
			  <div class="failure">
			    <span>Misserfolg</span>
				    <span class="readaloud">
						Ihr werft einen flüchtigen Blick in die Kisten, findet aber nur Schrott, der vermutlich als Ballast für die Kisten verwendet wird, um 
						sie als Barrikaden verwenden zu können.
						</span>
			  </div>
			</div>
		</div>
		<div>
    	<span>4 * J</span>
    	<span class="readaloud">
			Ihr nähert Euch dem Shuttle bis auf wenige Meter und steht vor der geöffneten Frachtluke, als daraus ein mörderisches Brüllen erklingt und
			ein Tiger-Beta mit gezogenen Waffen daraus hervorspringt und Euch angreift.	
			</span>
			<p>
			Die Justifiers, die an der Sammelherausforderungsrunde teilgenommen haben, müssen sich dem Angriff durch den Tiger-Beta stellen, der über die
			gleichen Werte verfügt, wie die anderen <a href="#ui-justifier">UI Justifiers</a> verfügt.
			</p>
			<p>
			Sollten die Justifiers im Kampf bewusstlos werden, verliert das Team eine Missionsrunde, in welcher der Arzt versuchen kann die bewusstlosen 
			Teammitglieder wieder fit zu bekommen. Bevor die Sammelherausforderung fortgesetzt werden kann, muss der Tiger-Beta besiegt werden.
			</p>
		</div>
		<div>
    	<span>5 * J</span>
			<p>Sie können den nachfolgenden Text vorlesen wenn Sie möchten oder ihre Justifiers selber entscheiden lassen, wie sie das Shuttle durchsuchen.
 			Passen sie an den entsprechenden Stellen den Text etwas an oder erzählen Sie frei, was die Justifiers finden. Es endet in jedem Fall im Cockpit.
			</p>
			<p>Falls die Justifers sich auf die Suche nach Vorräten machen, stellen sie fest, dass das ganze Shuttle scheinbar leer geräumt wurde. 
			Die Justifiers können hier <span class="emphasize">2 * J</span> Painkillerpatches finden.</p>
    	<span class="readaloud">
			Als ihr das Shuttle betretet, seid ihr in fahles, künstliches Licht getaucht, dass gegen die Helligkeit draußen so gut ankommt, wie eine Kerze 
			gegen die Sonne. Eure Augen brauchen eine ganze Weile um sich an die Verhältnisse anzupassen doch ihr rückt vorsichtig aber beständig weiter
			ins Innere des fremden Shuttles vor. 
			</span>
			<p class="readaloud">
			Ihr geht durch einen langen Korridor, welcher den leeren Frachtraum mit dem Cockpit verbindet. Rechts und links gehen ab und zu Türen ab. 
			Die ersten führen zu den  Mannschaftsräumen, die mit ca. 25 Doppelstock betten vollgestellt sind, die allerdings alle leer sind. Zumindest 
			im Moment.
			</p>
			<p class="readaloud">
			Ihr geht weiter in Richtung Cockpit und werft kurze Blicke in die weiteren Räume um sicher zugehen, dass sich niemand darin befindet. Dabei stellt
			ihr fest, dass das Shuttle sehr gut ausgestattet ist: Ein großes Labor, eine Krankenstation und eine riesige Werkstatt, in der sich nicht nur
			keine Gegner befinden, sondern auch keine Fahrzeuge.
			</p>
			<p class="readaloud">
			Schließlich erreicht ihr das Cockpit und entdeckt auf dem Sitz des Co-Piloten einen Menschen sitzen, der wild auf die Tastatur einschlägt. 
			Er dreht sich zu Euch um und ihr blickt in ein narbenüberzogenes Gesicht, aus dem Euch ein eisblaues, kaltes Auge ansieht. Das andere Auge
			ist von einer Augenklappe verdeckt. Er schaut Euch an und sagt "Ich liebe den Geruch von Napalm am Morgen!". Noch während der spricht, drückt er 
			ein weiteres Mal auf die Tastatur und aus den Lautsprechern dröhnt es "Selbstzerstörung des Shuttles in 10 ... 9 ... 8 ...". Noch bevor ihr 
			reagieren könnt nimmt er seine Pistole und erschießt sich.  
			</p>
			<p>Im finalen Kampf haben die Justifiers zwar einen Gegner weniger, aber jetzt heißt es Beine in die Hand nehmen. Fahren Sie nach Abschluss
			der folgenden Herausforderung bei <a href="#es-riecht-nach-sieg">Es riecht nach Sieg</a> fort.</p>
			<div class="challenge-box">
			  <div class="challenge">
			    <span class="dice">KÖRPER + ATHLETIK (3)</span>
			  </div>
			  <div class="success">
			    <span>Erfolg</span>	
						<p>Lesen Sie für alle Justifiers, die die Herausforderung bestanden haben den folgenden Text vor:</p>
				    <span class="readaloud">
						Ihr schafft es aus dem Shuttle herauszukommen und Euch hinter den Kisten in Sicherheit zu bringen, als hinter Euch die Hölle los bricht.
						Die Detonation zerreißt das Shuttle sprichwörtlich in abertausende Teile. Brennende Gegenstände fliegen um Euch herum in den Dschungel 
						und auf ihrem Weg alles in Brand.
						</span>
			  </div>
			  <div class="failure">
			    <span>Misserfolg</span>
						<p>Lesen Sie für alle Justifiers, die die Herausforderung NICHT bestanden haben den folgenden Text vor:</p>
				    <span class="readaloud">
						Ihr rennt die Laderampe herunter und wollt Euch hinter den Kisten in Sicherheit bringen, als das Shuttle hinter Euch zerrissen wird.
						Die Druckwelle befördert Euch weit mehrere Meter in den Dschungel und ihr schlagt hart auf dem Boden auf. Als ihr Euch berrappelt habt
						und wieder auf den Beinen steht, meint ihr, direkt in die Hölle katapultiert worden zu sein. Alles um Euch herum steht in Flammen und ihr
						habt jegliche Orientierung verloren.
						</span>
						<p>Die Justifier verlieren <span class="emphasize">-2 AP</span> und müssen zwei Kampfrunden aussetzen bevor sie am nachfolgenden Kampf
					  teilnehmen können</p>
			  </div>
			</div>
		</div>
	</div>
</div>

Falls die Justifiers es nicht schaffen, alle Stufen der Sammelherausforderung zu erreichen, lesen sie den folgenden Text vor und lassen sie die Gruppe
die beschriebene Herausforderung ausführen.

<div class="readaloud">
	<p>
	Als ihr wieder die Absturzstelle des UI Shuttles erkunden wollt, seht ihr einen Mann mit Augenklappe in der Frachtluke des Shuttles stehen, der sich 
	gerade eine Zigarre ansteckt und dann vom Shuttle wegschlendert als wäre er bei einem Spaziergang und genieße die frische Luft.
	</p>
	<p class="readaloud">
	Als er sich ca. 100m vom Shuttle entfernt hat, zeigt er, ohne sich umzudrehen, mit der linken Hand über die Schulter zum Shuttle und verfällt 
	anschließend in einen leichten Trab, während hinter ihm das Dröhnen eines Alarmsignals einsetzt. Aus Lautsprechern die ihr nicht sehen könnt ertönt 
	"Selbstzerstörung des Shuttles in 10 ... 9 ... 8 ...". 
	</p>
</div>

<div class="challenge-box">
	<p>Die Justifiers müssen die nachfolgende Herausforderung ausführen um zu prüfen, ob sie sich rechtzeitig in Sicherheit bringen können</p>
  <div class="challenge">
    <span class="dice">KÖRPER + ATHLETIK (3)</span>
  </div>
  <div class="success">
    <span>Erfolg</span>	
		<p>Lesen Sie für alle Justifiers, die die Herausforderung bestanden haben den folgenden Text vor:</p>
    <span class="readaloud">
		Ihr schaut Euch nach Verstecken um und entdeckt einige Container und große Bäume in der Nähe. Mit einigen beeindruckenden
		Sprüngen schafft ihr es Euch dahinter in Sicherheit zu bringen. Direkt danach bricht hinter Euch die Hölle los. Splitter des Shuttles 
		fliegen an Eurem Versteck vorbei in den Dschungel und stecken alles in Brand, mit dem sie in Berührung kommen.
		</span>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
		<p>Lesen Sie für alle Justifiers, die die Herausforderung NICHT bestanden haben den folgenden Text vor:</p>
    <span class="readaloud">
		Ihr versucht einen Unterschlupf vor dem drohenden Inferno zu erreichen als Euch die Explosion von den Füßen reißt und zusammen mit
		brennenden Trümmerteilen in Richtung Dschungel befördert. Ihr kommt hart auf und schlagt Euch verschiedene Körperteile schmerzhaft
		an den Hindernissen an, denen ihr nicht ausweichen könnt.
		</span>
		<p>Die Justifier verlieren <span class="emphasize">-2 AP</span> und müssen zwei Kampfrunden aussetzen bevor sie am nachfolgenden Kampf
	  teilnehmen können</p>
  </div>
</div>

### Es riecht nach Sieg

Die Justifiers müssen sich nun einer Gruppe von UI Justifiers stellen, deren Größe abhängig von den erreichten Stufen der vorherigen Sammelherausforderung ist.
Lesen Sie dazu folgenden Text vor

<div class="readaloud">
<p>Ihr steht mitten in einem flammenden Inferno. Der Rauch ist pechschwarz und undurchdringlich. In allen Richtungen in die ihr blickt, lodern die Flammen meterhoch in den Himmel. 
Nur ein schmaler Pfad von vielleicht 5 Meter Breite ist bisher vom Feuer verschont geblieben, aber ihr glaubt nicht, dass das lange so bleiben wird. Ihr schaut Euch nach euren Kameraden 
um und wollt Euch sammeln, als Euch plötzlich Geschosse um die Ohren fliegen. </p>
<p>Vom Pfad her kommen Gestalten auf Euch zu, die Gewehre im Anschlag und eröffnen das Feuer auf euch.</p>
</div>

Die Justifiers stehen <span class="emphasize">Anzahl Justifiers + 5 - erreichte Stufe in der Sammelherausforderung</span> UI Justifiers gegenüber, welche direkt angreifen. Die Justifiers
müssen sich also zuerst verteidigen, falls sie in der Lage dazu sind, bevor sie selbst angreifen können.

Sobald alle UI Gegner besieht sind, erhalten die Justifiers <span class="emphasize">10 MP</span>. Wenn Sie die Leichen untersuchen können Sie außerdem das 
Fundstück <a href="#ui-aufzeichnungen">UI Aufzeichnungen</a> finden. Sollten alle Justifiers bewusstlos werden oder die Gruppe sich zurückziehen wollen, 
können Sie jederzeit hierher zurückkommen und treffen dann auf die verbliebene Anzahl von Justifiern. 

<div class="enemy" id="ui-justifier">
	<div class="header">UI Justifiers</div>
	<ul>
		<li><span>Angriff</span>3/3/3</li>
		<li><span>Schaden</span>2</li>
		<li><span>Verteidigung</span>3</li>
		<li><span>Ausdauer</span>6</li>
		<li><span>Gefahr</span>2</li>
	</ul>
	<div>
		<span class="emphasize">Manöver</span>
		<span>
		Jeder UI Justifier greift einen Justifier aus dem Team an. Sind weniger Spieler am Kampf beteiligt als, UI Justifiers, greifen die 
		überzähligen UIs zuerst die Spieler Character mit den meisten Ausdauerpunkten an. 
		</span>
	</div>
	<div class="talent">
		<span class="header">Hilf mir Doc</span>
		<span>
		Falls die Gruppe auf UI-Betas trifft, die aus mindestens zwei Justifiern besteht, ist einer davon Arzt. Dieser greift die Gruppe nicht an, wenn 
		mindestens einer seiner Kameraden auf 1 AP gefallen ist, sondern versorgt dessen Wunden. Der verarztete Justifier erhält in dieser 
		Runde +2 AP, kann aber nicht angreifen. Arzt und UI Justifier können sich aber verteidigen. Der Arzt kann sich pro Runde nur um einen Patienten
		kümmern. Die Heilung erfolgt immer am Anfang der Runde, also vor allen Angriffen durch die Spieler.
		</span>
	</div>
</div>

## Szene 3: Friede sei mit dir

### Was hier passiert 

Die Justifiers finden eine Siedlung der Fa'Fa, Nachfahren der beiden Ur-Stämme, die gemeinsam und im Einklang miteinander leben.
Nach anfänglichen Verständigungsschwierigkeiten und dem Beweise ihrer Friedfertigkeit erfahren die Justifiers mehr über den Krieg zwischen den 
Baton'Fa und den Fa'Baton und über die Geschichte des Planeten. Außerdem wird ihnen von weiteren Eindringlingen berichtet, welche die Fa'Fa bedrohten
und ins Innere des heiligen Berges eindrangen um die Kammmer der Vorfahren zu öffnen.

### Das Dorf
<div class="readaloud">
Ihr erreicht den Waldrand am frühen Morgen und beginnt damit Euch langsam und vorsichtig durch den Wald in Richtung des Dorfes zu schleichen um die Bewohner 
nicht erneut zu verschrecken und Euch ein besseres Bild von Ihnen machen zu können. 
</div>

<div class="challenge-box">
  <div class="challenge">
		<span>Die Gruppe kann hier ihre besten "Schleicher" vorausschicken wenn nötig.</span>
    <span class="dice">Herausforderung auf GEIST + HEIMLICHKEIT (3)</span>
  </div>
  <div class="success">
    <span>Erfolg</span>
		<p class="readaloud">
		Ihr bewegt Euch langsam auf das Dorf zu und konzentriert Euch auf jede Eure Bewegungen. Schließlich findet ihr einen guten Platz, von dem aus ihr das Dorf beobachten könnt.
		</p>
		<p>Falls die Justifiers sich aufgeteilt haben, hat die Vorhut jetzt die Möglichkeit die restliche Truppe nachzuholen und über den Pfad zu führen, den sie soeben gefunden haben.
		Es muss dann keine Herausforderung bestanden werden.</p>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
		<span>Lesen sie das folgende Stück dem Justifier vor, der die Herausforderung nicht bestanden hat</span>
    <p class="readaloud">
		Du bewegst dich wie ein Schatten schnell von einem Versteck zum anderen. Das Unterholz ist hier allerdings so dicht, dass dich selbst deine Kameraden kaum sehen können. Da erreichst
		du eine Lichtung, schaust dich um und setzt zu einen kurzen Spurt an, als die Luft rein zu sein scheint. Du hast wenige Schritte gemacht, als dein Fuß
		plötzlich in die Höhe gerissen wird und du plötzlich kopfüber von einem Baum hängst. Während du da so hängst und versuchst deine Kameraden per JUST zu erreichen, hörst du in einiger Entfernung
		so etwas wie eine Glocke, die wie wild klingelt. 
		</p>
		<p>
		Das Überraschungsmoment ist dahin und die anderen Justifier müssen erstmal ihren unglücklichen Kameraden aus der Falle befreien. Die Justifiers verlieren 
		<span class="emphasize">-1 Missionsrunde</span>. Sollten die Justifiers versuchen wollen, dass Dorf zu betreten lesen Sie folgenden Text vor und lassen Sie sie das Anschleichen in der 
		nächsten Runde wiederholen.
		</p>
		<p id="fafa-village" class="readaloud">
		Ihr betretet den Boden unter dem Dorf und sucht nach einer Möglichkeit, nach oben zu kommen. Aber es findet sich weit und breit keine Leiter oder etwas ähnliches. Die Stämme der Bäume
		unterscheiden sich von allem was ihr bisher auf diesem Planeten gesehen habt und sind glatt wie polierter Stahl und sehr dick. Beim Versuch daran hochzuklettern merkt ihr, dass sie auch 
		so hart wie Stahl sind und ihr rutscht immer wieder ab. Aber klettern würde auch nicht viel bringen, denn am unterhalb der Plattformen befinden sich Kränze aus speerartigen Stangen, die das
		überklettern verhindern sollen. Ihr werdet hier ohne Hilfe nicht hochkommen. Eure einzige Option sind wohl die Bewohner des Dorfes.  
		</p>
  </div>
</div>

Sobald die vorige Herausforderung bestanden ist, lesen Sie folgenden Text vor:

<div class="readaloud">
<p>Ihr versteckt euch und beobachtet die Dorfgemeinschaft, die offensichtlich aus Individueen zweier Spezies besteht. Einerseits vogelartige Wesen, den Elfern nicht unähnlich, aber wesentlich größer
und mit Armen zusätzlich zu den Flügeln. Andererseits große, vierbeinige Wesen mit einem länglichen Körper dessen Oberfläche von Schuppen überzogen ist. </p>
<p>Es erscheint Euch seltsam, dass Echsen- und Vogelwesen eine Gemeinschaft bilden, aber sie leben offensichtlich miteinander und kommen gut miteinander aus.</p>

<p>Plötzlich hört ihr ein seltsames Geräusch hinter Euch und fahrt herum. Dort steht eines der Vogelwesen und direkt daneben eine der Echsen, die sich aufgerichtet hat und auf ihren 
Hinterbeinen steht. Beide haben die Arme bzw. Vorderbeine vor den Körpern verschränkt und blicken Euch auffordernd an.</p>
</div>


<div class="challenge-box">
  <div class="challenge">
		<span class="role">Die Herausforderung darf nur von einem Spieler abgelegt werden</span>
    <span class="dice">Herausforderung auf SEELE + CHARISMA (3)</span>
  </div>
  <div class="success">
    <span>Erfolg</span>
		<p class="readaloud">
		Du betrachtest Eure Gegenüber vorsichtig. Von ihnen scheint keinerlei Aggression auszugehen, aber sie scheinen etwas von Euch zu erwarten aber ihr habt noch keine Ahnung
		was. Du kramt das Wissen aus der Ausbildung für den Erstkontakt hervor und vergleichst das Auftreten eurer Gruppe mit dem der Dorfbewohner. Dabei fällt Dir auf, dass weder die beiden
		vor Euch noch die restlichen Dorfbewohner Waffen oder ähnliches tragen. Du machst eine, deiner Meinung nach, entschuldigende Geste und greifst langsam zu deiner Waffen, nimmst sie vorsichtig
		aus dem Holster und legst sie neben Dir ab. Eure Gegenüber nicken leicht und schauen nun Eure Kameraden an.
		</p>
		<p>Der Justifier erhält den Sonderbonus <span class="emphasize">"Ich bin wie Buddha"</span></p>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <p class="readaloud">
		"Was glotzen die beiden eigentlich wie die Ölgötzen? Können die sich vielleicht mal irgendwie äußern?" denkst du und zuckst mit den Schultern. Die beiden Einheimischen schauen sich an
		und deuten dann auf Eure Waffen und anschließend auf den Boden. Sie wollen anscheinend, dass ihr die Waffen ablegt. 
		</p>
  </div>
</div>

Die Justifiers müssen an dieser Stelle die Waffen ablegen, denn nur ohne Waffen werden sie ins Dorf gebracht. Sie dürfen sie aber noch verstecken, wenn sie wollen. Weigern sich die Justifiers
die Waffen abzulegen. Bleiben die beiden Dorfbewohner einfach vor ihnen stehen und schütteln den Kopf, bis sie ihnen Folge leisten. 

Sollten die Justifiers auf die selten dämliche Idee kommen, die beiden Dorfbewohner anzugreifen, ist das Abenteuer an dieser Stelle vorbei. Die restlichen Dorfbewohner werden sich verstecken. Die 
Vögel tragen die Echsen und fliegen in Richtung Fuß des Berges. Die Justifiers treffen die Dorfbewohner nicht mehr an und finden Sie auch nicht wieder. Falls die Justifiers es irgendwie schaffen
auf die Bäume zu kommen (siehe <a href="#fafa-village">Misserfolg</a> weiter oben), finden Sie nichts weiter Wichtiges in den Hütten außer Schlafplätzen und Kochstellen sowie einen Gemeinschaftsraum,
der aber leer ist.

<div class="readaloud">
<p>Nachdem ihr Eure Waffen abgelegt habt, gestikuliert der Vogel in Richtung des Dorfes. Anschließend macht er eine beruhigende Geste in Eure Richtung. Dann schlägt er mit den Flügeln, hebt
langsam vom Boden ab und fliegt über den Kopf der Echse neben sich. Diese streckt die Vorderbeine aus und greift nach den Füßen des riesigen Vogels über sich und wird von diesem hochgehoben.</p>
<p>Während ihr noch über dieses Zusammenspiel staunt, kommen weitere Vögel aus Richtung des Dorfes direkt auf Euch zugeflogen. Über jedem von Euch bleibt einer der Vögel in der Luft stehen und 
schlägt wie wild mit den Flügeln um die Position zu halten.</p>
</div>

Die Justifiers müssen nun nach den Füßen der Vögel greifen um sich von ihnen in das Dorf tragen zu lassen.

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf KÖRPER + ATHLETIK (2)</span>
  </div>
	<div>Lesen sie allen Justifiers, welche die Herausforderung bestanden haben, den Text für den Erfolg vor. Allen anderen den Text für den Misserfolg</div>
  <div class="success">
    <span>Erfolg</span>
		<p class="readaloud">
		Die Krallen schließen sich um Eure Unterarme wie Handschellen und ihr werdet mit einem Ruck in die Höhe gerissen und von den Vögeln in Richtung des Dorfes geflogen.
		</p>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <p class="readaloud">
		Ihr greift nach den Füßen des Vogels über Euch, erwischt sie aber nicht optimal. Als der Vogel die Krallen schließen will um Euch zu sichern, reißt euch die Kralle den Arm auf. 
		Ihr verliert <span class="emphasize">-1 AP</span>
		</p>
  </div>
</div>

<div class="readaloud">
<p>Die Vögel setzen Euch auf einer großen Plattform ab und bedeuten Euch anschließend eine der großen Hütten zu betreten. Ihr tut wie geheißen, denn ohne ihre Hilfe wird ein Abstieg 
von diesen Plattformen ziemlich schmerzhaft.</p>
<p>In dem großen Raum befinden sich zwei der Dorfbewohner, wieder ein Vogel und eine Echse. Sie unterhalten sich in einer Sprache die sich für Euch wie ein zischendes Krähen anhört.</p>
<p>Ihr könnt auf den ersten Blick erkennen, dass es sich hierbei nicht um die beiden Bewohner handelt, die Euch am Boden getroffen haben. Die Anwesenden sind wesentlich älter, das Gefieder
des Vogels ist dunkel blau mit etlichen ergrauten Stellen. Die Schuppen der Echse sind matt und an den Rändern rissig. Aber beide schauen Euch aus hellwach Augen an und bedeuten Euch näher
zu kommen und Platz zu nehmen.</p>
<p>Ihr bedankt Euch und setzt Euch vor die beiden, die scheinbar so etwas wie die Dorfältesten sind. Diese beginnen nun zu gestikulieren und in ihrer seltsamen Sprache zu reden.</p>
</div>

Die Justifiers sollten für den Plot mindestens 5 Erfolge erreichen. Schaffen sie das nicht in den 5 Runden, der Sammelherausforderung, werden Sie von den Gastgebern gebeten die Unterhaltung
am nächsten Tag fortzusetzen. Die Justifiers können in einer der Baumhäuser übernachten, verlieren <span class="emphasize">1 Missionsrunde</span> können dann aber die Herausforderung fortsetzen
und haben erneut 5 Runden. Dies kann solange wiederholt werden, bis die Justifiers 5 Erfolge erreicht haben.

<div class="challenge-box">
  <div class="challenge">
		<span class="role">Sammelstufenherausforderung für 5 Runden</span>
    <span class="dice">GEIST + WISSEN [LINGUISTIK]</span>
		<span class="dice">Sonderbonus "Ich bin wie Buddha" hilft</span>
		<span class="dice">Sprache der Elfer hilft. Bonus +1 </span>
  </div>
  <div class="collect-challenge-result">
    <div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
    <div>
      <span>1</span>
      <p class="readaloud">
			Ihr erfahrt, dass die Dorfgemeinschaft sich als "Fa'Fa" bezeichnet, was wohl irgendwas mit ihren Vorfahren zu tun haben muss, die vor 
			langer Zeit den Kontinent besiedelten. Ihr erfahrt auch, dass sie eine friedliche Gemeinschaft sind, die jüngst von einer Gruppe Wesen angegriffen wurde,
			die so aussahen wie ihr. Zumindest für die Augen der "Fa'Fa". 
      </p>
		</div>
    <div>
      <span>3</span>
      <p class="readaloud">
			Diese Gruppe verlangte von den Fa'Fa Informationen über die Umgebung und insbesondere über den See. Die Fa'Fa erzählten von den reichen
			Fischbeständen und leckeren Pflanzen, die am heiligen See wuchsen, aber das war wohl nicht das was die Angreifer wissen wollten, denn sie schlugen die Fa'Fa 
			und töteten einige von ihnen.  
			</p>
		</div>
		<div>
      <span>5</span>
			<div class="readaloud">
      	<p>
				Die Fa'Fa erzählten den Eindringlingen schließlich vom alten Staudamm, der den Zufluss aus dem Gebirge staut und durch den See die Versorgung der umliegenden Flora und Fauna sicherstellt.
				Entweder reichte das den Angreifern aus oder sie hatten genug von der anstregenden Unterhaltung. Jedenfalls ließen Sie von den Gefangenen ab und gingen ihrer Wege. Natürlich nicht ohne vorher
		  	das komplette Dorf auf den Kopf zu stellen.
				</p>
				<p>
				Ihr ermutigt die Dorfältesten dazu Euch mehr über den Staudamm zu erzählen. So erfahrt ihr, dass der Staudamm älter ist als die Vorfahren der Fa'Fa, den Fa'Baton und Baton'Fa. 
				Die Legenden erzählen, das der Staudamm das Herz des Kontinents ist, dass alles mit Leben versorgt. Außerdem sollen die Vorfahren den Zugang zu diesem Herz verschlossen und 
				den Schlüssel in Einzelteile zerlegt haben. Jeder Stamm bekam einen Teil der Schlüssel, so dass nur alle zusammen zum Herz hätten vordringen können. 
				</p>
				<p>Die Justifiers erhalten <span class="emphasize">+1 Geschichtspunkt</span></p>	
			</div>
    </div>
    <div>
      <span>10</span>
      <span class="readaloud">
			Wo die Stämme sich niedergelassen hatten oder wieviele es überhaupt waren, können die Dorfältesten nicht sagen, nur dass beide Rassen aus mehrere Stämme hatten. Ihr erfahrt aber 
			mehr darüber, wie ihre Vorfahren der Fa'Fa sich vom Krieg zwischen den Baton'Fa und Fa'Baton abwendeten um eine friedliche Koexistenz der beiden Rassen aufzubauen. Und wie 
			diese dabei in Ungnade bei ihren Brüdern fielen und von beiden Seiten gleichermaßen verachtet und bekämpft wurden.  Der Stamm der Fa'Fa konnte erst wachsen, nach dem die beiden 
			Ur-Spezies ausgestorben waren. 
      </span>
			<p>Die Justifiers erhalten <span class="emphasize">+1 MP</span> </p>
    </div>
		<div>
			<span>15</span>
			<span class="readaloud">
			Die Fa'Fa zeigen den Justifiern alte Karten der Umgebung, insbesondere vom heiligen See, der an den Rand des Waldes grenzt. Sie erzählen von der reichen Flora und Fauna,
			welche die Fa'Fa ernährt. Interessant wird es aber erst, als sie von alten unterirdischen Gängen sprechen, die die Vorfahren anlegten und unter dem See verschiedene
			Edelmetalle abbauten. Die Zugänge dazu sind zwar auf den Karten eingezeichnet, aber laut Eurer Gastgeber seit langer Zeit eingestürzt.
			</span>
			<p>Die Justifiers erhalten <span class="emphasize">+1 MP</span> </p>
		</div>
	</div>
</div>

Es ist nun die Aufgabe der Justifiers herauszufinden, was die anderen "Besucher" der Fa'Fa taten, nach dem Sie vom Staudamm erfuhren. Außerdem müssen sie ggf. die Schlüsselteile finden um sich 
Zugang zum "Herz des Kontinents" zu verschaffen. 

Die Justifiers können sich im Dorf noch ein wenig umsehen, finden aber nichts von Bedeutung. Die Fa'Fa werden ihnen aber freundlich begegnen, wenn die Justifiers ihnen auch freundlich gesinnt sind.
In dem Fall bekommen Sie auch ihre Waffen direkt im Dorf übergeben.

Wenn sie das Dorf verlassen wollen, setzen die Fa'Fa sie direkt unter dem Dorf ab. Wenn sie fragen, können sie von den Fa'Fa aber auch bis in die Nähe des Staudamms geflogen werden.
Die Fa'Fa fliegen sie aber nicht direkt zum Ziel, sondern setzen sie etwas vorher ab um nicht die gegnerischen Justifiers auf sich aufmerksam zu machen.

### Der Staudamm

<div class="readaloud">
Das ihr Euch dem Staumdamm nähert, merkt ihr daran, dass das Tösen des herabstürzenden Wassers immer lauter wird. Und schließlich könnt ihr den Staudamm in einiger Entfernung sehen.

Die Mauer liegt zwischen zwei Felswänden, die an der Innenseite so glatt sind, als hätte jemand mit einem riesigen Laser einfach eine dünne Scheibe aus dem Berg geschnitten. Wobei "dünn"
wohl nicht das richtige Wort ist und nur im Verhältnis zu dem tausende Meter hohen Gebirge diesen Eindruck erweckt. Tatsächlich ist die Staumauer wohl ca. einen Kilometer breit. Unter der Krone
befinden sich 8 große Öffnungen, die als Überlauf dienen, wenn die Zuflüsse zum See zu viel Wasser liefern.

Auf der anderen Seite der Krone befindet sich ein kleines Haus, in dem ihr den Eingang zum Damm vermutet.  
</div>

Die Justifiers müssen sich nun zum Haus begeben und müssen dazu die Dammkrone überqueren, offenes und ungeschütztes Gelände. Sie können aber NICHT durch den See schwimmen, zumindest nicht in der 
Nähe der Mauer, da die Strömung viel zu stark wäre. Sie könnten aber in einige Entfernung durch den See schwimmen und das Gebäude über die andere Seeseite erreichen. Es bleibt dem Erzähler
überlassen, wie schwer die Herausforderung dafür ist und ob die Ausrüstung beim Schwimmen Schaden erleidet und ggf. für das Abendteuer unbrauchbar ist.

<div class="readaloud">
Kurz bevor ihr das Haus erreicht, fliegt dessen Tür auf und ein Stier Beta stürmt mit einer Vibroklinge in der Hand auf Euch zu. Aus dem Haus heraus schießen zwei weitere Gegener mit Gewehren auf
Euch.
</div>

Die Werte der beiden Schützen entsprechen denen der Standard <a href="#ui-justifier">UI Justifiers</a> nur mit einem Verteidigungswert + 1 durch die Deckung des Hauses. Der Stierbeta hat
keine Fernkampf-Waffen, also nur einen Angriffswert 3/0/0 und beginnt auf RW2.

Die Justifiers müssen die UIs erst besiegen bevor sie den Staudamm betreten können und können die Justifiers nur dann als Angehörige von UI identifizieren, wenn sie bereits Szene 2 gespielt haben.

<div class="readaloud">
<p>Als ihr die Überreste des Stier-Beta untersuht, blinkt sein JUST auf und zeigt an, dass er eine neue Nachricht erhalten hat und ihr lasst sie abspielen.</p>

<div style="background-color: darkgrey">
<p>Gerald, wir sind auf dem Weg zum Shuttle um die großen Spielzeuge zu holen. Dann hatten diese Hinterwäldler mal einen Damm aber wir kommen an die Schätze 
unter dem See ran. Ich mach mir nur bissel Sorgen, dass der Sergeant diesem irren Franky erlaubt hat eine große Bombe zu bauen. Der Typ ist genauso instabil wie das was er da immer zusammenbaut.</p>

<p>Jedenfalls wird es wohl noch ein bisschen dauern, bis wir wir zurückkommen. Du sollst solange die Stellung halten und dich sofort melden, wenn irgendwas interessantes passiert.
Der Sergeant meint, du sollst Bescheid sagen "bevor" du irgendwas unternimmst. Er hat wohl Angst dass du wiedermal erst drauf haust und erst dann fragst, wie bei den Eingeborenen.
Also halt die Ohren steif und bleib wachsam.</p>
</div>

<p>Ihr sucht im JUST nach weiteren Informationen findet aber nur noch ein paar Koordinaten ohne weitere Angaben darüber, was sich dort befindet.</p>
</div>

Decken Sie den Spielern Erkundungsziel I auf, falls nicht bereits geschehen. Haben die Justifiers Szene 2 bereits gespielt, so weisen Sie sie darauf hin, das es sich bei den 
Koordinaten um die Position des Shuttles der anderen Justifiers Truppe handelt. Sie brauchen sich dann eher keine Sorgen machen, dass die anderen UIs es schaffen den Damm zu sprengen. 
Fahren sie mit dem Abschnitt <a href="#haus-auf-der-staumauer">Haus auf der Staumauer</a> fort.

Ist Szene 2 noch nicht gespielt worden, <span style="font-weight:bold">können</span> Sie den Justifiers an dieser Stelle freistellen, das Abenteuer hier zu unterbrechen um die Gegner zu verfolgen. Die Spieler verlassen dann den 
Abenteuer Modus und kehren zum Shuttle zurück. Anschließend wird normal im Explorationsmodus weitergespielt bis Szene 2 beendet ist. Anschließend können sie bei <a href="#haus-auf-der-staumauer">Haus auf der Staumauer</a> weitermachen.

### Haus auf der Staumauer

Das Haus ist zum Teil an die Felswand gebaut in welcher eine Tür eingelassen ist. Dahinter befindet sich ein Gang der gerade aus weiter in den Fels führt und geradeaus ist eine steile Treppe 
die durch den Staudamm nach unten führt. An der Wand sind Schilder angebracht, die die Justifiers aber nicht lesen können.

Die Justifiers müssen sich jedes Mal, wenn sie hierherkommen, entscheiden, welchen Weg sie gehen wollen. Gehen Sie den Gang entlang fahren sie mit 
<a href="#vorraum-zum-herz-des-kontinents">Vorraum zum Herz des Kontients</a> fort. Andernfalss mit <a href="#turbinenhaus">Turbinenhaus</a>

### Vorraum zum Herz des Kontinents

<div class="readaloud">
<p>Der Gang endet in einem großen Raum der offenbar nur für die riesige Tür gebaut wurde, welche sich links von Euch befindet. Von den anderen Wänden scheint ein ambientes, 
dezentes Licht auszugehen, in dessen Schein ihr die kunstvollen Ornamenten erkennen könnt, welche präzise in den geschliffenen Stein eingearbeitet wurden, aus dem die Flügel 
der Tür bestehen.</p>

<p>Einen Griff oder Öffnungsmechanismus sucht ihr vergebens, aber in der Mitte der Tür erkennt ihr eine Vertiefung in einer unregelmäßigen Form.</p>
</div>

Die Justifiers haben das Tor zum Herzen des Kontinents gefunden, welches sich nur öffnen lässt, wenn die Justifiers alle Schlüsseteile gefunden und zusammengesetzt haben. Fahren Sie in dem Fall
mit <a href="#szene-4-herz-des-kontinents">Szene 4</a> fort. 

### Turbinenhaus

<div class="readaloud">
<p>Ihr erreicht das Ende der Treppe und öffnet die Tür, die dort seitlich abgeht. Kaum habt ihr sie geöffnet, wird aus dem beständigen Brummen, dass ihr bereits beim Abstieg vernommen habt, ein 
ohrenbetäubendes Dröhnen. Das stammt von den drei Paar, riesiger Turbinen und Generatoren, die sich unter dem Gitterboden vor Euch befinden.</p>

<p>An der gegenüberliegenden Wand befindet sich ein hell erleuchteter Raum, der durch eine Tür und ein breites Fenster vom spärlich erleuchteten Maschinenraum getrennt ist. 
Das bisschen Licht, was es Euch ermöglicht grobe Umrisse der Maschinen zu erkennen, stammt von einigen elektronischen Anzeigetafeln. Ihr könnt die Zeichen darauf nicht entziffern, vermutet
aber, dass es sich um Leistungsdaten der Turbinen handelt.</p>
</div>

#### Maschinenraum

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Stufenherausforderung auf GEIST + TECHNIK [ANLAGEN]</span>
		<span class="role">Ein anderer Justifier kann helfen und 1 Erfolg beisteuern, wenn folgende Herausforderung erfolgreich ist</span>
    <span class="dice">Herausforderung auf GEIST + WISSEN [LINGUISTIK] (2)</span>
  </div>
	<div class="collect-challenge-result">
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>1</span>
			<p class="readaloud">
			Es sind drei Anzeigetafeln und drei Turbine-Generator-Einheiten. Also beobachtet ihr die beiden Anzeigen eine zeitlang. Und tatsächlich verändert sich die linke Anzeige
			ab und zu, während die mittlere immer den geleichen Wert zeigt. Auf der Anzeigetafel blinken immer wieder die gleichen Zeichen in roter Schrift auf. Ihr vermutet Generator 3 ist unrettbar
			und nur noch Schrott. 
			</p>
  	</div>
  	<div>
    	<span>2</span>
			<p class="readaloud">
			Ihr untersucht die anderen beiden Aggregate genauer und könnt Eure Annahme bestätigen, dass nur eines der beiden tatsächlich läuft. Ihr vermutet, dass es sich um redundante
			Einheiten handelt.  
			</p>
  	</div>
  	<div>
    	<span>4+</span>
			<p class="readaloud">
			Nach genauerer Betrachtung erkennt ihr an der inaktiven Turbine eine rot blinkende Leuchte, welche an der anderen durchgängig rot leuchtet. Vermutlich ist die inaktive Einheit defekt.
			Ihr habt das Gefühl hier gute Vorarbeit für das Nachschubteam geleistet zu haben und erfasst alle Erkenntnisse ordnungsgemäßt in Eurem Erkundungslogbuch. 
			</p>
			<p>Die Justifiers erhalten <span class="emphasize">+2 MP</span> </p>
  	</div>
	</div>
</div>

Die Justifiers, die nicht an der Untersuchung der Turbinen mithelfen, können ggf. auch parallel schon in den Steuerungsraum gehen, der sich an der gegenüberliegenden Wand befindet.

#### Steuerungsraum

<div class="readaloud">
Ihr öffnet die Tür und betretet den Raum dahinter. Dieser ist klein und enthält weitere Anzeigetafeln sowie ein großes Steuerpult, mit verschiedenen Reglern. An der fensterlosen Wand steht eine
Art Schreibtisch auf der sich eine Art Tastatur befindet und unter dem Tisch ein kleiner grauer Kasten. 
</div>

Die Justifiers sollten zuerst versuchen den Computer unter dem Schreibtisch zum Laufen zu bringen und nachfolgende Herausforderung bestehen, bevor sie sich an die anschließende Herausforderung
machen.

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + COMPUTER (3)</span>
  </div>
  <div class="success">
    <span>Erfolg</span>
		<p class="readaloud">
		Ihr schafft es den Computer zu starten und ein Holo-Display flackert auf dem Tisch auf. Nach einiger Zeit könnt bekommt ihr auch die Steuerung halbwegs in den Griff und könnt einige der 
		Dateien öffnen, die sich darauf befinden. Neben einigen Dokumenten, die auf Euch den Eindruck von Betriebsanleitungen machen, findet ihr eine Zeichnung von einem seltsam geformten, 
		länglichem Objekt. Besonders interessant scheint die Querschnittszeichnung zu sein. Sie stellt dar, dass das Objekt aus 6 veschiedenen einzelteilen zusammengesetzt wird. Jedes Segment ist in 
		einer anderen Farbe dargestellt. blau, gelb, rot, lila, grün und orange.
		</p>
		<p>Die Justifiers erhalten <span class="emphasize">+1 MP</span></p>
		<p>Sobald einem Justifier auffällt, dass die Form der Querschnittsdarstellung, der des "Schlüssellochs" in der Tür im Vorraum zum Herz des Kontinents entspricht, erhalten dieser die Karte 
		<span class="emphasize">Meisterpuzzler</span></p>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <p class="readaloud">
		Nach ein paar Tastendrücken, startet der Computer und ein Holo-Display erscheint. Ihr durchsucht die verschiedenen Einträge und öffnet einige der dargestellten Elemente. Aber ihr findet
		nichts von Interesse.
		</p>
  </div>
</div>

Die Justifiers können sich noch etwas umsehen wenn sie möchten oder, falls sie noch nicht dort waren, zum Vorraum zum Herz des Kontinents. Falls sie das Tor dort bereits gefunden und untersucht
hatten, endet die Szene an dieser Stelle und die Justifiers sollten sich auf die Suche nach den Schlüsseln.

## Szene 4: Herz des Kontinents

### Was hier passiert 

Nachdem die Justifiers die Teile des Schlüssels für das Tor zum Herz des Kontinents gefunden und zusammengesetzt haben, kommen sie zurück in den Vorraum zum Herz des Kontinents. Mithilfe des
Schlüssels öffnen Sie das Tor und steigen auf zum Terraforming Modul, das von den Fa'Fa als Herz des Kontinents bezeichnet wurde. Auf dem Weg dorthin müssen sie verschiedene Gefahren bestehen 
um am Ende das Terraforming Modul zu sichern. 

### Macht auf!

Falls die Justifiers hier mit den Einzelteilen des Schlüssels auftauchen und sie noch nicht zusammengesetzt hatten, sollten Sie sie erst die Herausforderungen ausführen lassen, die beim 
Fundstück <a href="#schlüsselteile">Schlüsselteile</a> aufgeführt sind.

<div class="readaloud">
Endlich, nach den Strapazen, die ihr für die Suche nach den Schlüsselteilen auf Euch genommen habt, steht ihr nun vor dem Tor. Nun gilt es den zusammengesetzten Schlüssel auch zu benutzen und 
endlich zum Herz des Kontinents vorzustoßen. 
</div>

Lassen Sie die Spieler die folgenden Schritte selbst herausfinden und die Szene ausspielen. Falls sie nicht auf die Idee kommen, lassen Sie sie eine Herausforderung auf 
<span class="emphasize">SEELE + WAHRNEHMUNG</span> würfeln, entscheiden Sie selbst über den Erfolgsgrad.

<ol>
	<li>Der Schlüssel muss ins Schloss</li>
	<li>Der Schlüssel muss zuerst nach links gedreht werden</li>
	<li>Der Schlüssel muss anschließend nach rechts gedreht werden</li>
	<li>Zuletzt muss der Schlüssel noch einmal weiter ins Schlüsselloch geschoben werden</li>
</ol>

Sobald die einzelnen Stufen erfolgreich waren lesen sie folgenden Text vor:

<div class="readaloud">
<p>Es knackt kurz und in Euren Ohren klang das gar nicht gut ... aber dann leuchten plötzlich die Lichter in den Wänden hinter Euch heller und schließlich öffnen sich die riesigen 
Flügel der Tür.</p>
<p>
Dahinter liegt ein dunkler Gang in dem ihr nur Treppenstufen erkennen könnt. Das sieht nach einem langen, langen Aufstieg aus. 
</p>
</div> 

Die Justifiers müssen nun mehrere hundert Höhenmeter zu Fuß überwinden. Die Justifiers müssen solange würfeln, bis sie ALLE Erfolgsstufen der unteren Tabelle
erreicht haben. Es würfeln alle Justifiers und die Erfolge aller Justifier in einer Runde addieren sich zu denen der vorherigen Runden.
Lesen sie in jeder Runde die jeweils neu erreichten Stufen vor. (passen Sie die Schwierigkeit ggf. an, so dass die Spieler bei Laune bleiben)

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Sammelherausforderung auf KÖRPER + ATHLETIK</span>
    <span class="role">Alle 5 Runden verlieren die Justifiers einen Ausdauerpunkt</span>
  </div>
	<div class="collect-challenge-result">
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>1</span>
			<p class="readaloud">
			Ihr betretet die ersten Stufen und stellt Euch schon den erhabenen Moment vor, wenn ihr das Herz des Kontinents gleich erreicht haben werdet.
			</p>
  	</div>
  	<div>
    	<span>3+</span>
			<p class="readaloud">
			Ok, es scheinen doch ein paar mehr Stufen zu sein. 
			</p>
  	</div>
  	<div>
    	<span>10</span>
			<p class="readaloud">
			Einen riesigen Staudamm bauen, aber keinen Fahrstuhl. Was stimmt nur mit den Leuten nicht ...
			</p>
  	</div>
  	<div>
    	<span>15</span>
			<p class="readaloud">
			Ihr habt den Eindruck, dass das Treppenhaus irgenwie heller wird. Das scheint Tagesslicht zu sein ...
			</p>
  	</div>
  	<div>
    	<span>20</span>
			<p class="readaloud">
			Ja Tageslicht. Durch ein Fenster .... Naja der Ausblick ist ganz nett hier. Ihr seid ca. 300 Meter über dem Erdboden.
			</p>
  	</div>
  	<div>
    	<span>25</span>
			<p class="readaloud">
			Endlich eine Tür. Das Treppenhaus endet in einer Tür und ihr versucht diese zu öffnen ...
			</p>
  	</div>
	</div>
</div>

Die Tür ist offen, klemmt aber, da sie seid Jahren nicht bewegt oder gewartet wurde. Aber das muss man den Justifiers ja nicht auf die Nase binden :-)
Lassen sie die Justifiers selbst nach einer Lösung suchen. Aber hier hilft tatsächlich nur rohe Gewalt.

<div class="readaloud">
<p>Mit einem Knall öffnet sich die Tür. Naja eigentlich gibt der Türrahmen nach und fällt nach draußen. Heimliches Vorgehen ist was anderes, aber manchmal muss man halt
mit dem Kopf durch die Wand.</p>

<p>Jedenfalls steht ihr nun oben im Gebirge auf einer Art Hochplateu über dem ein riesiger Felsüberhang thront. Das ist alles was ihr seht, den in der Felsnische darunter herrscht so dichter Nebel,
dass ihr nicht erkennen könnt was sich dahinter verbirgt. </p>
</div>

Die Justifiers können sich noch etwas umsehen, wenn sie wollen. Scanner könnten helfen zu erkennen, dass hinter dem Nebel eine Art Gebäude liegt, dass eine erhebliche Energiesignatur abstrahlt

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf SEELE + WAHRNEHMUNG (2)</span>
  </div>
	<div class="readaloud">
		Ihr bewegt Euch vorsichtig in den Nebel hinein, einen Schritt nach dem anderen. Der Nebel ist kalt und wird immer dichter, als ihr unter den Überhang tretet.
	</div>
  <div class="success">
    <span>Erfolg</span>
		<p class="readaloud">
		Je weiter ihr in den Nebel tretet desto, wärmer wird es und entsprechend lichtet sich der Dunst auch ganz langsam. Dafür setzt langsam ein Luftstrom ein der aber beständig stärker wird. 
		Gerade als ihr das ansprecht und die Gruppe stoppt, bläst Euch ein starker Luftstoß fast von den Beinen. Ihr könnt aber die Balance gerade noch halten.
		</p>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <p class="readaloud">
		Ihr geht immer weiter in die Nebel hinein, der sich mit jedem Schritt langsam lichtet. Aber plötzlich schlägt Euch ein so starker Luftstoß entgegen, dass ihr das Gleichgewicht verliert 
		und zur Seite ausweichen müsst. Doch der Schritt geht ins Leere und ihr stürzt.
		</p>
		<p>Der Justifier verliert <span class="emphasize">-1 AP</span></p>
  </div>
</div>

<div class="readaloud">
<p>Rechts und links des Weges den ihr gekommen seid, befinden sich große Rohrleitungen die scheinbar direkt aus dem inneren des Berges hierher führen und scheinbar warme Luft hierher transportieren. 
Allerdings könnt ihr nicht sagen, wozu die Rohre oder die warme Lufte dienen. </p>

<p>Schließlich erreicht ihr ein Gebäude, das direkt in den Berg gebaut zu sein scheint. Die Wände bestehen aus Glas oder einem ähnlichen Material. Da es aber verspiegelt ist, könnt ihr nicht hindurchsehen. Der Weg zwischen den Rohren führt direkt auf eine Tür zu, über der eine rote Leuchte brennt. </p>
</div>

Egal was die Justifiers versuchen, es ist vorgesehen, dass sie durch die Rohre kriechen um ins innere des Gebäudes zu kommen. 
Sollten die Justifiers allerdings massive Geschütze auffahren, können Sie entscheiden, wie die Justifiers in das Gebäude kommen. Fahren sie dann entsprechend weiter unten fort.

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Stufenherausforderung auf COMPUTER + TECHNIK</span>
		<span>Lesen sie alles bis zum erreichten Erfolgsgrad vor</span>
  </div>
	<div class="readaloud">
		Aufgrund des Dunstes in den Rohren seht ihr kaum die Hand vor Augen im Schein eurer Lampen.
		Ihr startet ein paar allgemeine Scanprogramme auf eurem JUST um die Umgebung zu scannen und insbesondere den Abstand zur nächsten Wand zu vermessen. 
	</div>
	<div class="collect-challenge-result">
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>1</span>
			<p class="readaloud">
			Auf dem JUST baut sich langsam eine Karte auf, die einen Umkreis von 10 m um Euch herum darstellt und euch den Verlauf des Rohres zeigt. 
			Dieses verläuft in direkte Linie zum Gebäude.
			</p>
  	</div>
  	<div>
    	<span>3+</span>
			<p class="readaloud">
			Das JUST meldet eine enorme Dichte mikroskopisch kleiner Lebensformen hier unten in den Rohren. Ihr fokusiert die Scanner auf die nähere Untersuchung diese Mikroben. Es scheint
			sich im Wesentlichen um Baktieren unterschiedlicher Art zu handeln. Präzisere Aussagen würden sich nur mithilfe eines Labors treffen lassen. Das ihr gerade nicht bei Euch habt. 
			</p>
			<p>
			Die Justifiers erhalten das Fundstück <a href="#mikroben">Mikroben</a>.
			</p>
  	</div>
	</div>
</div>

<div class="readaloud">
Ihr bewegt Euch langsam vorwärts und erreicht schließlich ein Gitter, welches vermutlich das Eindringen von kleinen Tieren verhindern soll. Nun verhindert es auch erstmal euer Weiterkommen.
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf KÖRPER + ATHLETIK (4)</span>
		<span class="role">Waffen wie HardGrip V2 helfen</span>
		<span class="role">zweiter Justifier kann einen Erfolg beisteuern</span>
		<span>Alternative: Gitter aufschießen. Querschläger können die Justifiers treffen, die dann <span class="emphasize">-1 AP</span> verlieren. Spielleiter Entscheidung</span>
  </div>
	<div class="readaloud">
		Das Gitter ist an vier Stellen mit den Rohren verschweißt, ein einfaches Abmontieren ist nicht  möglich. Ihr könntet es sicherlich zerschießen, aber das würde Krach machen und 
		die Gefahr von Querschlägern ist zu groß. Also ist hier erstmal Muskelkraft gefragt.
	</div>
  <div class="success">
    <span>Erfolg</span>
		<p class="readaloud">
		Das Metall quietscht unter der schieren Gewalt die auf das Gitter einwirkt. Aber nach einiger Zeit reißt eine Verbindung nach der anderen ab und der Durchgang ist endlich frei.
		</p>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <p class="readaloud">
		Deine Muskeln in den Armen sind fast zum Zerreißen gespannt und vor Anstrengung treten dir die Schweißperlen auf die Stirn. Aber das Gitter beweget sich scheinbar keinen Millimeter.
		Du hast alles gegeben aber sackst vor Erschöpfung in dich zusammen und lässt dich am Gitter hinabgleiten. Du brauchst einen Moment um dich auszuruhen.
		</p>
		<p>Alle an der Herausforderung beteiligten Justifier verlieren <span class="emphasize">-1 AP</span> können es aber anschließend nochmal versuchen.</p>
  </div>
</div>

<div class="readaloud">
	<p>Ihr pasiert das Gitter und folgt dem Verlauf des Rohres	einige Meter. Schließlich tretet ihr aus dem Rohr in einen großen, ebenfalls runden Gang aus dem viele weitere Rohre abgehen. 
	Die Luft hier ist wärmer und der Nebel nicht mehr so dicht und es gibt Licht. Künstliches Licht, das von Lampen an der Wand auf euch fällt.</p>
  <p>Als ihr Euch gegenseitig betrachtet, schimmern eure Rüstungen in einem seltsamen Licht. Bei genauerer Betrachtung erkennt ihr, dass sich eine Art Patina auf der Rüstung gebildet hat,
	die sich mit bloßen Händen auch nicht abwischen lässt.</p>
</div>

Es spielt keine Rolle ob die Justifiers nach links oder nach rechts gehen. An den Enden des Ganges befindet sich immer eine Leiter.

<div class="readaloud">
	Ihr erreicht das Ende des Ganges und findet eine lange Leiter, die nach oben führt. Ihr beginnt mit dem Aufstieg als ihr von oben Geräusche vernehmt.
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf SEELE + WAHRNEHMUNG [HÖREN] (2)</span>
  </div>
	<div class="readaloud">
		Ihr haltet Inne und lauscht den Geräuschen von oben. 
	</div>
  <div class="success">
    <span>Erfolg</span>
		<p class="readaloud">
		Es klingt als würde etwas über den Boden kriechen oder schleifen, aber es scheint sich langsam oder schwerfällig zu bewegen.
		</p>
		<p>Die Justifiers erhalten die Teamkarte <a href="#wie-ein-ninja">Sonderbonus: Wie ein Ninja</a></p>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <p class="readaloud">
		Als ihr stoppt um zu lauschen, endet auch das Geräusch abrupt. Ihr wartet einen Moment aber entweder ist das, was das Geräusch verursacht hat, weg oder es hat aufgehört sich zu bewegen.
		</p>
  </div>
</div>

### Der Graben

Die Justifiers müssen hintereinander die Leiter erklimmen und sollten sich vorsichtig nach oben bewegen. Sollten sie das nicht berücksichtigen kürzen sie das folgende ab und fahren direkt
mit dem Kampf fort. 

<div class="readaloud">
	<p>Als die Leiter endet, befindet ihr Euch mit dem Rücken zu einem schmalen Gang, in den von oben herab Licht duch ein Gitter fällt.
	Durch die Löcher in den Gittern erblickt ihr unzählige Rohrleitungen, die von der Decke aus ins Zentrum eines runden Raumes verlaufen um dort in einem Zylinder zu enden, der 
	den Großteile des Raumes einnimmt.</p>
	<p>
	Der Zylinder besteht aus durchsichtigem Material und ist scheinbar von innen beleuchtet. Bei genauerer Betrachtung scheint es sich um eine Art Tank zu handeln, in dem eine Art blau-grünes, zähflüssiges
	Plasma schwimmt.
	</p>
	<p>Ihr befindet Euch in einem Graben, der mittig, unterhalb eines breiteren Ganges verläuft, der um den Zylinder herumführt. An der innenliegenden Wand des Grabens verlaufen weitere Leitungen.
	Von der Decke bis zum Boden winden sich rote und blaue Rohre. An der außenliegenden Wand erkennt ihr hier und dort verschiedene Kabel, die von unten nach oben führen und die darüberliegende
	Phalanx von Steuerpulten, Anzeigetafeln und Computerterminals mit Energie versorgen. 
	</p>
</div>

Die Justifiers sollten sich nun in dem Gang umsehen und nach einem Aufgang oder ähnlichem Suchen. Dabei sollten sie vorsichtig vorgehen, da sie nicht wissen, was über ihnen vorgeht.

<div id="wip"/>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Sammelstufenherausforderung auf SEELE + WAHRNEHMUNG</span>
		<span>Die Justifiers können beliebig viele Runden würfeln, müssen aber in jeder Runde zusätzlich eine Herausforderung auf <span class="emphasize">GEIST + HEIMLICHKEIT (1)</span> bestehen. 
		Justifiers, die dies nicht schaffen, müssen stehen bleiben und dürfen nicht mehr an der Stufenherausforderung teilnehmen.</span>
  </div>
	<div class="readaloud">
	Ihr verständigt Euch mit Zeichensprache den Gang entlang zu gehen und nach einem Aufgang zu suchen. Ihr bewegt euch langsam und vorsichtig um nicht den Urheber der Geräusche von vorhin 
	auf Euch aufmerksam zu machen.
	</div>
	<div class="collect-challenge-result">
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>3</span>
			<p class="readaloud">
			Jetzt hört ihr wieder die Geräusche, die scheinbar von dem Gang über Euch kommen aber scheinbar auf der anderen Seite des Zylinders. Es klingt wie ein lautes Schmatzen.
			</p>
  	</div>
  	<div>
    	<span>5</span>
			<p class="readaloud">
			Ihr seid nicht weit gegangen, als ihr an der Seite des Grabens eine weitere Leiter findet, die nach oben führt. Dort scheint man das Gitter nach oben wegklappen zu können um in den Gang 
			darüber zu kommen.
			</p>
			<p>
			Die Justifiers können hier nach oben gehen oder sich den Graben noch weiter anschauen.
			</p>
  	</div>
  	<div>
    	<span>10</span>
			<p class="readaloud">
			Während ihr an den Gang weiterentlang geht, scheint sich die Temperatur hier unten zu verändern, obwohl kein Luftzug oder ähnlches zu spüren ist. Einer Vermutung folgend, berührt ihr die Rohre 
			an den Innenwänden und spürt dass die roten Rohre warm und die blauen kalt sind. Scheibar handelt es sich hier um eine Art Klimaanlage. Aber für was? Ihr vermutet, dass ihr das hier im Graben
			nicht herausfinden werdet.
			</p>
  	</div>
	</div>
</div>

### Der Glubsch

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + HEIMLICHKEIT (2)</span>
		<span>Alle Justifiers legen die Herausforderung nacheinander</span>
		<span>Erster Justifier </span><span class="emphasize">Malus -1</span> da ihm keiner helfen kann.
  </div>
	<div class="readaloud">
		Ihr klettert die Leiter hinauf und öffnet die Klappe ganz vorsichtig. 
	</div>
  <div class="success">
    <span>Erfolg</span>
		<p>Für alle bis auf den letzten Justifier
		<span class="readaloud">
			Du schaffst es durch die Klappe und stehst im Gang und hilfst deinem Kameraden nach dir 
		</span>
		</p>
		<p>Für den letzten Justifier
		<span class="readaloud">
			Du erreichst den Gang über dem Graben als letzter und schließt die Klappe vorsichtig hinter dir.
		</span>
		</p>
		<p>Wenn alle Justifiers die Herausforderung bestehen, lesen sie außerdem folgenden Text vor:</p>
		<p class="readaloud">
		Ihr schleicht langsam um den Zylinder herum bis ihr plötzlich auf der anderen Seite ein riesiges, formloses, blaues Wesen erspäht, das am Zylinder zu kleben schien und dessen Oberfläche sich in 
		pulsierdenden Wellen bewegt. Es hat Euch scheinbar noch nicht bemerkt. 	
		</p> 
		<p>
		Die Justifiers haben nun die Möglichkeit sich abzustimmen was sie tun wollen und sie haben die Initiative im Falle eines Kampfes, können also zuerst angreifen.
		</p>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <p class="readaloud">
		Du rutschst an der Leiter ab und fällst zurück in den Graben. Da die Leiter nicht sehr hoch war, verletzt du dich nicht, machst aber einen Höllenlärm.
		Sobald Ruhe eingekehrt ist, hört ihr wieder das schmatzende Geräusch, welches jetzt näher kommt. Hinter dem Zylinder kommt ein riesiges, blaues, umförmiges Wesen hervor und greift Euch an. 
		</p>
		<p>Die Heimlichkeit ist vorbei und die Justifiers klettern alle schnell nach oben. Die Klappe fällt hinter ihnen zu. Der Glubsch hat die Initiative und greift die Justifiers zuerst an.</p>
  </div>
</div>

Die Justifiers können den Glubsch nicht besiegen, solange der "flüssig" ist. Sie müssen die Klimanlage im Graben nutzen um den Glubsch zu gefrieren. Wie sie das anstellen, bleibt ihnen überlassen.
Sie könnten die Leitungen zerschießen, eine Tretmine anbringen und sprengen oder die Computer anlage hacken und die Klimanlage steuern. 
Sobald der Glubsch gefroren ist, kann er mit gewöhnlichen Waffen zerstört werden oder die Justifiers lassen ihn einfach gefroren zurück. 

<div class="enemy">
	<div class="header">Der Glubsch</div>
	<ul>
		<li><span>Angriff</span>2/2/2</li>
		<li><span>Schaden (biologisch)</span>2</li>
		<li><span>Verteidigung</span>0 wenn gefroren, sonst unendlich</li>
		<li><span>Ausdauer</span>1</li>
		<li><span>Gefahr</span>1</li>
	</ul>
	<div>
		<span class="emphasize">Manöver</span>
		<span>
		Der Glubsch greift mit gallertartigen Geschossen an, welche Teile seines Körpers sind. Deshalb kann er auch alle Justifiers angreifen.
		Die Geschosse verursachen bei Einschlag 1 AP biologischen Schaden und 1 AP kinetischen Schaden. Die <a href="#mikroben">Mikroben</a> 
		können helfen um den biologischen Schaden zu zu neutralisieren.
		</span>
	</div>
	<div class="talent">
		<span class="header">Flutsch und weg</span>
		<span>
		In jeder zweiten Runde verschwindet der Glubsch durch die Gitter im Boden in den Graben und einen Justifier von unten an, in dem er aufsteigt und den Justifier absorbiert. 
		Dieser kann erst wieder in den Kampf eingreifen, wenn der Glubsch erneut "abtaucht". 
		</span>
	</div>
	<div class="talent">
		<span class="header">Tausend Stücke</span>
		<span>
		Im gefrorenden Zustand ist der Glubsch so spröde, dass ein einziger Schuss ausreicht um ihn kleinste Teile zu sprengen. Bei Energiewaffen wird er verdampfen. 
		</span>
	</div>
</div>

<div class="readaloud">
	Das war ja eklig. Ihr habt das Gefühl das ein Teil des glubschigen Wesens immer noch an Euch klebt, in eurem Fell und eurer Rüstung und ihr sehnt Euch nach einer Dusche. 
	So habt ihr jetzt das erste Mal Zeit Euch diesen seltsamen Raum genauer anzusehen. Dabei entdeckt ihr, dass dort wo sich das Wesen bei Eurer Ankunft befand ein winziges Leck in dem Tank mit dem Plasma
	ist. Ihr vermutet, dass die Masse aus dem Tank mit irgendetwas reagiert hat und sich so zu dem Wesen entwicklet hat, dass sich eben im ganzen Raum verteilt hat.
</div>

Die Justifiers erhalten nach dem Sieg über den Glubsch <span class="emphasize">+5 MP</span>

Nach dem Kampf gegen den Glubsch können sich die Justifiers umsehen und ggf. den Raum und den Tank in der Mitte untersuchen.

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Stufenherausforderung auf</span>
		<span class="dice"> SEELE + WAHRNEHMUNG</span>
		<span class="dice"> GEIST + COMPUTER</span>
		<span class="dice"> GEIST + WISSEN [BIOLOGIE]</span>
		<span>
		Die Justifiers können sich aussuchen auf welche Herausforderung sie würfeln wollen.
		</span>
  </div>
	<div class="readaloud">
	Ihr schaut Euch um und untersucht die Computer und den Tank mal genauer.
	</div>
	<div class="collect-challenge-result">
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>2</span>
			<p class="readaloud">
			Nichts hier drin sieht aus, als hätten es die Fa'Fa oder ihre Vorfahren gebaut. Das ist alles viel, viel älter. 
			</p>
  	</div>
  	<div>
    	<span>5</span>
			<p class="readaloud">
			Das kann nur Ancient-Technologie sein. So etwas fortschrittliches kann nur von den Ancients geschaffen worden sein. Es handelt sich hier 
			um den Teil eines Terra-Forming Moduls. 
			</p>
			<p>
			Die Justifiers erhalten <span class="emphasize">+5 MP</span>
			</p>
  	</div>
  	<div>
    	<span>10</span>
			<p class="readaloud">
			Der Tank enthält Bakterien und andere Mikroorganismen, die darin gezüchtet, dann in einem Aerosol gelöst und durch die Luft über den ganzen Kontinent verteilt wird.
			Dies stabilisiert die Flora und Fauna auf dem Kontinent und reichert das Ökosystem an. Wenn man herausfindet, wie man das auch auf anderen unwirtlichen Planeten verwenden kann, 
			steht der Erschließung dieser Gesteinsbrocken nichts mehr im Weg und erleichter deren Ausbeutung.
			</p>
			<p>
			Die Justifiers erhalten <span class="emphasize">+10 MP</span>
			</p>
  	</div>
	</div>
</div>

## Abreise oder Spielende

<div class="todo">beschreiben was passiert, wenn die Justifiers abreisen oder das Spiel endet</div>

## Karten

<div class="todo">Das müssen echte Karten zum Ausdrucken sein. Alternativ roll20 Handouts verwenden</div>

### Sonderbonus-Karten

<div class="card">
<div class="header">Sonderbonus: Ich bin wie Buddha</div>
<p class="readaloud">Durch deine überlegte und besonnene Art beim Erstkontakt mit den Fa'Fa hast du direkt einen Stein im Brett bei den Ureinwohnern von Zone 3.</p>
<p>Der Justifier mit dieser Karte erhält auf alle <span class="emphasize">SEELE</span> Herausforderungen zu Aktionen mit den Fa'Fa einen Bonus von <span class="emphasize">+1</span>
Der gleiche Bonus gilt für Herausforderungen auf <span class="emphasize">GEIST + WISSEN [LINGUISTIK]</span></p>
</div>

<div class="card">
<div class="header">Sonderbonus: Meisterpuzzler</div>
<p class="readaloud">Puzzeln war schon immer dein Hobby, egal ob 5 oder 40.000 Teile, 2D oder 3D, Spielzeug oder Waffenteile. Sobald du Teile in der Hand hälst, versuchst du 
sie zusammen zu setzen.</p>
<p>Der Justifier mit dieser Karte erhält auf alle <span class="emphasize">GEIST + TECHNIK</span> Herausforderungen beim Versuch die Teile des Schlüssels zum Herz des Kontinents 
zusammen zu setzen einen Bonus <span class="emphasize">+1</span>
Der gleiche Bonus gilt für Herausforderungen auf <span class="emphasize">SEELE + WAHRNEHMUNG</span> bei der Untersuchung möglicher Schlüsselteile.</p>
</div>

<div class="card">
<div id="wie-ein-ninja" class="header">Sonderbonus: Wie ein Ninja</div>
<p class="readaloud">Vorgewarnt durch die Geräusche über Euch stellt ihr Euch auf alles mögliche ein und geht vorsichtiger vor.</p>
<p>Alle Justifier erhalten innerhalb des Herz des Kontinents für alle Herausforderungen auf <span class="emphasize">GEIST + HEIMLICHKEIT</span> einen Bonus von 
<span class="emphasize">+1</span></p>
</div>

## Fundstücke

### Tretminen

Das Team kann beim United Industries Shuttle eine Tretmine einsammeln. Diese könnten dafür eingesetzt werden, die Anzahl der UI Justifiers zu 
reduzieren, in dem Sie die Tretminen umprogrammieren und an anderen Stellen vergraben. 

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + TECHNIK [ELEKTRONIK] (2)</span>
  </div>
	<div class="readaloud">
    Deine Kameraden schauen sich nach einem sicheren Versteck um, in dem sie in Deckung gehen können, als du beginnst dich über die Mine zu 
		beugen und sie mit der Software auf deinem JUST umzuprogrammieren. Sie zucken bei jedem Piep zusammen, dass aus deiner Richtung kommt. 
	</div>
  <div class="success">
    <span>Erfolg</span>
		<p class="readaloud">Doch schlussendlich schaffst	du es die Mine umzuprogammieren. Sie deaktiviert sich jetzt, sobald eines eurer JUST 
		in umittelbarer Nähe ist. Bei UI Truppen tut sie das nun nicht mehr. Ihr könnt die Mine jetzt ausbuddeln und an anderer Stelle vergraben.</p>
		<p>Die Justifiers können sich jetzt im Wald verstecken und auf das Eintreffen von UI Justifiern warten oder weiter machen. Wenn sie möchten können 
		Sie es beliebig ausschmücken, dass einer der UI Betas durch die Mine ausgeschaltet wird. In jedem Fall reduziert sich die Anzahl der gegnerischen Justifier
		im finalen Kampf um eins</p>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <p class="readaloud">
		Das Piepen der Mine wird schneller. Irgendeine deiner letzten Aktionen war wohl etwas unglücklich. Du versucht noch schnell in Deckung zu gehen, bevor die Mine dich zerreißt, 
		fällst dabei aber über deine eigenen Füße. Glücklicherweise detoniert die Mine nicht, aber du bekommst einen ordentlichen Stromschlag ab.
		</p>
		<p>Der Justifier verliert <span class="emphasize">-1 AP</span></p>
  </div>
</div>

### Granatwerfer

Das Team kann bei der Untersuchung des Umfelds des UI Shuttles einen United Industries Granatwerfer finden und mit ins Inventar aufnehmen. Das Problem
ist allerdings, dass der Granatwerfer ausschließlich mit Granaten von United Industries funktioniert. Granaten aus dem Arsenal von Gauss Industries kann
nicht als Ersetz verwendet werden und führt zur Zertörung des Granatwerfers und enstprechenden Schaden bei dem jenigen, der es versucht.

Im Feld können die Justifiers nur rausfinden, dass es ein Granatwerfer ist und wo der Abzug ist. Mehr ist da auch erstmal nicht interessant. Aber falls 
sie das gute Stück später nochmal genauer unter die Lupe nehmen, müssen sie folgende Herausforderung ausführen

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Sammelherausforderung auf GEIST + TECHNIK</span>
  </div>
	<div class="collect-challenge-result">
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>1</span>
			<p class="readaloud">
				Ihr betrachtet das gute Stück mal genauer. Es ist ganz offenstichtlich ein Granatwerfer, ähnlich dem <span class="emphasize">Catapult</span> 
				von Gauss Industries, aber eine eigentständige Waffe. Das geht auch nicht anders, denn die Granaten die man hier verschießt sind um ein 
				Vielfaches größer, als alle Granaten, die euer Arbeitgeber herstellt. 
			</p>
  	</div>
  	<div>
    	<span>3+</span>
			<p class="readaloud">
				Bisher ist Euch von einer derartigen Waffe noch nichts zu Ohren gekommen und die Datenbank findet auch nichts dazu. Es kann gut sein, dass ihr 
				hier einen Prototypen von United Industries in den Händen haltet. Das dürfte für Gauss mehr als interessant sein und Euch einen fetten Bonus 
				einbringen. 
				<span class="emphasize">+2 MP</span>
			</p>
  	</div>
	</div>
</div>

<div class="weapon">
	<dl>
		<dt>Bezeichnung</dt><dd>Hot Potato</dd>
		<dt>AP</dt><dd>14</dd>
		<dt>Schlagwort</dt><dd>Waffe</dd>
		<dt>Fertigkeit</dt><dd>Fertigkeit</dd>
		<dt>NAH</dt><dd>-</dd>
		<dt>RW1</dt><dd>+3</dd>
		<dt>RW2</dt><dd>+2</dd>
		<dt>Besonderheiten</dt><dd>Anwendungen(3), Explosiv, nur mit UI Munition verwendbar</dd>
	</dl>
</div>

### UI Aufzeichnungen 

<div class="readaloud">Bei einem der besiegten UI Justifiers findet ihre einige Notizen. Diese sind tatsächlich auf die altmodische Art mit Papier und Bleistift 
verfasst worden. Die wenigen Blätter die nicht blutgetränkt oder rußverschmiert sind, sind aufgrund der schlampigen Handschrift 
kaum zu entziffern. Soweit ihr sagen könnt, geht es im Wesentlichen um den Kontinent und zwei alte Rassen, welche den Planeten
vor langer Zeit bevölkerten. Die Aufzeichnungen enthalten außerdem Hinweise auf Orte von Siedlungen dieser Stämme auf dem Kontinent.</div>

Die Justifiers erhalten <span class="emphasize">+1 Geschichtspunkt</span>

### Schlüsselteile

bei der Analyse/dem zusammenbau hilft die Karte "Meisterpuzzler"

### Mikroben

<div id="mikroben" class="challenge-box">
  <div class="challenge">
    <span class="dice">Sammelherausforderung auf GEIST + WISSEN [BIOLOGIE] </span>
  </div>
	<div class="collect-challenge-result">
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>1</span>
			<p class="readaloud">
			Die Mikroorganismen auf Eurer Rüstung scheinen eine Verbindung mit dem Metall und dem Kunststoff eingegangen zu sein. 
			</p>
  	</div>
  	<div>
    	<span>3+</span>
			<p class="readaloud">
				Die Mikroben verhindern, dass andere biologische Stoffe mit der Rüstung interagieren können. Tatsächlich scheinen sie sich sogar in einem begrenzten Maße über Öffnungen 
				zu legen und schützen somit auch den Träger. Aber ihr könnt nicht sagen, wie lange das so bleibt. 
				<span class="emphasize">+2 MP</span>				
			</p>
			<p>Die Rüstungen der Justifiers erhalten für die Dauer ihres Aufenthalts auf Holz 11 eine Bonuspunkt auf den Verteidigungswert gegen biologische Waffen.</p>
  	</div>
	</div>
</div>

## Notizen

In der Nähe der Siedlung (Erkundkungsziel I) befindet sich ein Staudamm, der einen Zufluss aus den Bergen staut so dass der See in Binnensee II entstand.
Unter dem See befindet sich eine Mine, in welcher sich massive Vorkommen von Bodenschätzen finden lassen. 

Von den Fa'Fa erfahren die Justifiers, dass die Vorfahren den Zugang zum Terraformning Modul gesichert haben und dieser nur mit einem speziellen Schlüssel zu öffnen ist. Dieser 
Schlüssel wurde in mehrere Teile zerlegt und über den ganzen Kontinent verteilt versteckt. Die Justifiers müssen sich auf die Suche danach machen um das Terra-Forming Modul erschließen zu können

Im Herz des Kontinents treffen sie auf den Blob, eine Lebensform, die aufgrund einer Fehlfunktion in der Terraforminganlage entstanden ist und sich von den dort generierten Bakterien etc. 
ernährt. 


<del>UI Justifiers versuchen den Damm zu sprengen 
um einen Zugang zu den Bodenschätzen zu schaffen. 
Der Damm ist aber die Energiequelle für das Terraforming Modul von Kontinent 2 und das Sprengen des Dammes würde zum Stopp des Terraformings führen, was eine Verwüstung
des Kontinents nach sich ziehen würde. </del>

<del>Aber die UIs haben keinen Sprengstoff mehr und suchen in den Regionen nach Material um Sprengstoff herzustellen. Die Gruppe muss versuchen die UIs aufzuhalten 
und den Kontinent zu retten oder aber selbst den Damm zu sprengen. In den Regionen gibt es jeweils Stoffe um Sprengstoff herzustellen.
Die Stoffe könnten aber auch als alternative Energiequelle genutzt werden um das Terraforming Modul solange zu betreiben, dass man den Staudamm soweit leer lassen kann um einen Zugang
zu den Bodenschätzen zu schaffen und danach den Staudamm wieder anzustauen. </del>
