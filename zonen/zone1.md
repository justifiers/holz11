---
layout: mission
title: 'Zone 1'
---

# Kontinent 1

Der Aufbau und die Beschreibung von Kontinent 1 sowie die Abenteuer, welche die Justifiers dort erleben, 
sind im Basisregelwerk beschrieben. Da es das immer noch für einen vernünftigen Preis zu kaufen gibt und 
ich gar nicht das Recht habe das zu veröffentlichen, werde ich das hier nicht beschreiben. Vielleicht habe
ich irgendwann mal Zeit eine alternative Geschichte zu schreiben, aber aktuell ist das nicht notwendig.

Hilfreich wäre aber sicher, wenn möglich, dass die Elfer den Justifiers, von ihren Göttern erzählen. 
Insbesondere, wenn sie die Götterprüfung bestanden oder viele Kontaktpunkte bekommen haben:

<div class="readaloud">
<p>Die Elfer erzählen Euch von ihren Göttern, welche die Elfer nach ihrem Ebenbild geschaffen haben sollen. 
Wundersame Maschinen sollen die Götter bessen haben, die so mächtig waren, dass Sie über Leben und Tod 
entschieden und die Götter zu den himmlischen Richtern machten, welche die Elfer jetzt verehren.</p>

<p>Aber wie in den meisten bekannten Religionen, scheint es auch einen Gegenspieler der Götter zu geben.
Eine riesige geschuppte Kreatur, mit einem langen Maul und glühenden Augen, welche das Böse in jeder
Kreatur erkennen und sie verleiten wollen, den Untergang der Elfer-Götter herbeizuführen um allein über
den Planeten und die Sterne zu herrschen.</p>
</div>
