---
layout: mission
title: 'Zone 2'
---

# Kontinent 2 (Wüste)

![Zonenaufbau von Kontinent 2. Alle Zonen sind Wüsten. Die Randgebiete sind rund um eine zentrale Region angeordnet](../images/K2_Zonen.png)

## Bildnachweise

* [Wüste by Falkenpost](https://pixabay.com/de/users/Falkenpost-1987955/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1654439)

## Downloads 

* [Regionskarten Wüste](../images/Regionskarten_Wueste.pdf)
* [Zonenreferenzblätter](../images/K2_Zonenreferenzblaetter.pdf)

## Was hier passiert

Die Justifiers treffen auf dem Wüstenkontinent ein und stellen fest dass sie den Kontinent nicht überqueren 
sondern nur an der Küste landen können. Die interessanteste Region im Zentrum des Kontinents ist mit 
dem Shuttle nicht zu erreichen. Auf dem Weg dorthin begegnen Sie riesigen wurmartigen 
Kreaturen die sich durch die Dünen der Wüste graben (Ähnlichkeit zu "Dune - der Wüstenplanet" ist Absicht 
&#x1f607; aber das müssen die Spieler ja nicht wissen).

Die Spieler müssen sich etwas einfallen lassen, wie sie an die Energiequelle im Zentrum des Kontinents 
kommen, welche Teil einer riesigen Anlage eines früheren Volkes ist. Die Spieler werden dabei mehr über die 
Geschichte des Planeten erfahren und wie es zur Verwüstung des Kontinents kam.

## Szene 1: Roundabout

Die Justifiers erreichen den Kontinent bei Tag. Falls sie aus irgendeinem Grund in der Nacht starten wollen,
verzögert ein defektes Bauteil am Shuttle die Abreise oder die Reise dauert länger als gedacht.

<div class="readaloud">
<p>
Als ihr auf die Küste des neuen Kontinents zusteuert und langsam Land in Sicht kommt, 
bemerkt ihr die starke Hitze die auf der Oberfläche herrschen muss. Die Luft flirrt durch die aufsteigende 
warme Luft so stark, dass ihr eigentlich nur unterschiedliche Brauntöne erkennen könnt.
</p>

<p>
Die extreme Wärme beeinflusst selbst einige der Bordscanner wie ihr erstaunt feststellt, als ihr beginnt 
den Kontinent vom Shuttle aus untersuchen zu wollen. Ihr könnt lediglich eine starke Energiequelle orten, 
die sich scheinbar Zentrum des Kontinents befindet. Das ist aber erstmal eurer kleinere Sorge als ihr 
feststellt, dass auch andere Instrumente von den klimatischen Verhältnissen beeinflusst werden. Das ist nix 
für Anfänger am Steuerpult ...
</p>
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Für den Pilot:</span>
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
  </div>
  <div class="success">
    <span>Erfolg</span>
    <span>Die Justifiers können weiterfliegen.</span>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <span>Die Justifiers sind gezwungen in der Region zu landen an der sie gerade vorbeifliegen. Weiter 
		bei </span>
		<a href="#notlandung">Notlandung</a>
  </div>
</div>

Die Justifiers können nun beginnen den Kontinent zu umkreisen und zu scannen, wobei sie sich entlang 
der Küstenregionen bewegen ohne diese tatsächlich zu betreten. Dabei wiederholen sich die Piloten- und 
nachfolgende Scan-Herausforderung für jede Regionskarte an der Sie vorbeikommen.

Legen Sie deshalb für die Visualisierung der Position eine Markierung neben die jeweilige Regionskartei, aber 
decken Sie sie nur auf, wenn die Herausforderung erfolgreich abgeschlossen wurde. Die Justifiers erhalten 
in diesem Fall <span class="emphasize">+1 MP</span> für die Erkundung, müssen aber dennoch den
Regionsscan ausführen wenn sie die Region betreten.

Beginnen Sie an der nördlichen Spitze des Kontinents (Wüste I). Es spielt dabei keine Rolle ob die Justifiers
auf der westlichen oder der östlichen Seite des Kontinents entlang fliegen, sie können den Kontinent aber 
nicht überqueren. Sollte der Pilot es dennoch versuchen, fordern Sie eine Probe auf 
<span class="emphasize">GEIST + PILOT</span> ohne den Erfolgswert bekannt zu geben, 
denn diese Herausforderung soll nicht bestanden werden. Fahren Sie in diesem Fall bei 
<a href="#notlandung">Notlandung</a> fort.

Die nachfolgende Scan-Herausforderung wird abwechselnd mit der Pilotenherausforderung für jede angeflogene 
Region ausgeführt. Lesen Sie bei erfolgreicher Scan-Herausforderung jeweils den Text für die aktuelle Region 
vor. Fahren Sie bei einer fehlgeschlagenen Pilot-Herausforderung mit <a href="#notlandung">Notlandung</a> fort.
Bei einer fehlgeschlagenen Scan-Herausforderung erklären Sie den Justifiers, dass die Scanner nichts finden
können und lassen Sie sie zur nächsten Region fliegen.

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Für den Hauptnavigator:</span>
    <span class="dice">Herausforderung auf GEIST + COMPUTER (2)</span>
  </div>
<div class="collect-challenge-result">
  <div class="header"><span>Region</span><span>Auswirkung</span></div>
  <div>
    <span><a href="#wüste-i">Wüste I</a></span>
    <span class="readaloud">
			Als ihr näher kommt und die Sicht etwas besser wird, erkennt ihr, dass die Küste in diesem Bereich eine 
			massive, steile Felswand ist, die sich gute 100m fast senkrecht aus dem Wassser erhebt. In Eurem 
			Sichtbereich findet ihr nichts, was einen Schatten wirft oder sich als Landeplatz anbietet. Der Sensor 
			für die Lebensbedingung blinkt rot und meldet, dass humanoide Lebensformen hier tagsüber nur mit 
			Spezialausrüstung länger als 5 Minuten überleben. Aber auch die Anzeigen des Shuttles zeigen deutlich, 
			dass es nicht für einen längeren Einsatz im Zentrum der Wüste gedacht ist. Insbesondere nicht nach dem 
			Umbau zum Portal.
    </span>
  </div>
  <div>
    <span><a href="#wüste-ii">Wüste II</a></span>
		<span class="readaloud">
			Das wilde Flackern des Sensors für Lebensbedingungen scheint immer wilder zu werden. Erstaunlicherweise 
			meldet sich nun ein weitere Scanner und behauptet, dass es auf diesem Stück Stein mit Sandüberzug 
			tatsächlich etwas zu leben scheint. Aber das Signal ist so schwach, dass es kaum vom Rauschen zu 
			unterscheiden ist. 
		</span>
	</div>
  <div>
		<span><a href="#wüste-iii">Wüste III</a></span>
		<span class="readaloud">
			Weder die Sensoren noch eure Augen, die ihr aufgrund der massiven Helligkeit zusammengekniffen habt,
	 		scheinen hier irgendetwas spannendes zu finden.
		</span>
	</div>
  <div>
	  <span><a href="#wüste-iv">Wüste IV</a></span>
		<span class="readaloud">
			Nun scheint auch der Geologie Sensor etwas gefunden haben. Die Messwerte deuten auf Vorkommen von 
			Salpeter, Kupfer, Silber und sogar Gold hin. Na da wird sich Gauss aber freuen. Und ihr freut Euch 
			über einen weiteren Missionspunkt. 
		</span>
		<span class="emphasize">+1 MP</span>
	</div>
  <div>
		<span><a href="#wüste-v">Wüste V</a></span>
		<span class="readaloud">
			Auf euren Scannern ist die Energiequelle im Zentrum des Kontinents deutlich zu erkennen. Aber zwischen 
			Euch und diesem vermutlich lohnenswerten Ziel liegt eine umbarmherzige Wüste an der Küste und 
			dahinter ein riesiges Gebirge. Das Shuttle schafft des niemals dorthin, aber auch bei den andere 
			Fahrzeugen seid ihr Euch da nicht so sicher. 
		</span>
	</div>
  <div>
		<span><a href="#wüste-vi">Wüste VI</a></span>
		<span class="readaloud">
			Mittlerweile nehmt ihr das unablässige Blinken des Lebensbedingungs-Sensors schon nur noch am Rande 
			war und konztriert Euch auf die Anzeigen der anderen Sensoren. Aber leider ohne Erfolg ... doch als ihr 
			aus dem Fenster seht, meint ihr eine Bewegung im Sand zu erkennen. Doch als ihr es Euch genauer ansehen 
			wollt, ist nichts dergleichen zu erkennen. Habt ihr jetzt schon Halluzinationen? 
		</span>
	</div>
</div>
</div>

Falls die Justifiers eine komplette Umrundung des Kontinents geschafft haben und tatsächlich überlegen einen 
anderen Kontinent aufzusuchen, erinnern Sie sie daran, dass das ein neuer Zonenwechsel wäre und sie somit 
weitere Missionsrunden verlieren werden. Und um hierher zurück zu kommen, müssen Sie dann erneut einen 
Zonenwechsel durchführen. Aber es ist die Entscheidung der Spieler.

Haben sich die Justifiers für eine Landung entschieden fahren sie mit <a href="#touchdown">Touchdown</a> fort.

### Notlandung

<div class="readaloud">
	<p>
		Mist. Das Shuttle beweget sich nun unaufhaltsam auf die sandige Oberfläche des Kontinents zu. 
		Euch bleibt nur noch, soviel Schaden wie möglich zu verhindern. Euch geht nur noch ein Gedanke 
		durch den Kopf: "Hoffentlich bleibt das Portal heil."
	</p>
	<p>
		Das Shuttle schlägt hart auf und rutscht unkontrolliert durch den Sand. Die Geräusche die dabei 
		entstehen klingen für Euch irgendwie nach einem alten Registrierkasse, welche die verursachten 
		Schäden aufnimmt und auf Euren Buyback addiert. Als ihr endlich zum Stehen kommt ist es dunkel, 
		denn nur die Notbeleuchung funktioniert noch.
	</p>
	<p>
		Das Shuttle gilt als <span class="emphasize">stark beschädigt</span> und muss repariert werden bevor 
		es wieder starten kann.
	</p>
	<p>
		Weiter bei <a href="#befreiung">Befreiung</a>
	</p>
</div>

### Touchdown

<div class="readaloud">
  <p>
		Stolz setzt der Pilot das Shuttle auf der sandigen Oberfläche des Kontintents auf und dreht sich grinsend
	  zu Euch um. "Na was sagt ihr dazu? Eine butterweiche Landung." sagt er freudig. Da hat er recht, aber 
		der Boden scheint auch so weich zu sein wie Butter. 
  </p>
	<p>
		Ihr merkt wie das Shuttle langsam im Boden versinkt und vom Sand verschluckt wird. Nach einer gefühlten 
		Ewigkeit stabilisiert sich euer Gefährt. Es ist dunkel im Innenraum, so dass ihr nur noch durch die 
		Notbeleuchtung etwas erkennen könnt.
	</p>
	<p>
		Das Shuttle gilt als <span class="emphasize">beschädigt</span> und muss repariert werden bevor es wieder 
		starten kann.
	</p>
	<p>
		Weiter bei <a href="#befreiung">Befreiung</a>
	</p>
</div>

### Befreiung

Egal wie die Justifiers auf dem Boden aufgekommen sind, lesen Sie nun folgenden Text.

<div class="readaloud">
	Ok hier sitzt ihr in der Klemme. Aber auf der Plusseite ist es jetzt nicht mehr so unerträglich heiß und
	ihr bekommt noch für eine ganze Weile Luft, schließlich seid ihr in einem Shuttle gefangen, welches für die
	Reise in planetaren Orbits bestimmt ist. Auch das Portal scheint grundsätzlich zu funktionieren. Aber ihr 
	müsst hier raus und das Shuttle wieder an die Oberfläche bekommen bevor die Energiereserven aufgebraucht
	sind. 
</div>

Die Justifiers müssen sich mit Hilfe der nachfolgenden <span class="emphasize">Sammelherausforderung</span> 
aus dem Sand befreien da die Triebwerke nicht anspringen werden. Lassen Sie die Gruppe aber ruhig erst ein 
wenig diskutieren wie sie hier raus kommen können, ggf. passen sie die Herausforderung ein wenig an. Die 
Beschreibung der erreichten Stufen müssen dann ggf. etwas umformuliert werden. 

<span class="emphasize">Die Justifiers verlieren in jeder Runde einen Ausdauerpunkt.</span>
Sind alle am Graben beteiligten Justifiers bewusstlos, müssen sie mindestens 1 Missionsrunde aussetzen um 
wieder zu Kräften zu kommen. Anschließend setzen sie das Graben bei der bereits erreichten Anzahl an Erfolgen
fort.

<div class="challenge-box">
  <div class="challenge">
		<span class="role">Sammelherausforderung auf</span>
    <span class="dice">KÖRPER + SURVIVAL [WÜSTE] (Anzahl Justifiers x 4)</span>
  </div>
  <div class="success">
    <span>Erfolg</span>
    <span class="readaloud">Ihr erreicht die Oberfläche an der es Nacht und bitterste Kälte ist. Ihr habt 
		Euch noch nie so sehr gefreut zu frieren.</span>
  </div>
  <div class="failure">
    <span>Misserfolg</span>
    <span class="readaloud">Ihr schleppt Euch zurück ins Shuttle, wo ihr vor Erschöpfung zusammenbrecht 
		und direkt einschlaft. Ihr müsst erstmal ausruhen bevor ihr weitermachen könnt.</span>
  </div>
</div>

Das Shuttle gilt als beschädigt und erst repariert werden bevor die Justifiers einen Zonenwechsel durchführen
können (siehe Seite 208 BRW). 

## Exploration

Die Justifiers können die Exploration <span class="emphasize">nur nachts</span> durchführen, da wegen der 
Sandwürmer keine Fahrzeuge genutzt werden können (siehe permanente Regionsereignisse) und die 
Temperaturen ein Reise zu Fuß während des Tages nicht erlauben. 

### Sandwürmer

Unabhängig davon welche Region sie erkunden, werden die Justifiers von den Sandwürmern angegriffen, wenn Sie 
Gegenstände (z.B. Fahrzeuge, Ausrüstung, Waffen, ...) mit sich führen, die ein elektrostatisches Feld erzeugen.
Die Stärke des Feldes ist dabei egal, außer für die Auswahl des angegriffenen Justifiers bzw. Objekts. Die 
einzige Ausnahme bildet die Region in der das Shuttle gelandet ist, da dort ein massiver Untergrund 
existiert den selbst die Würmer nicht durchdringen können.

<div class="enemy">
	<div class="header">Sandwurm</div>
	<ul>
		<li><span>Angriff</span>8/0/0</li>
		<li><span>Schaden</span>4</li>
		<li><span>Verteidigung</span>4</li>
		<li><span>Ausdauer</span>20</li>
		<li><span>Gefahr</span>8</li>
	</ul>
	<div>
		<span class="emphasize">Manöver</span>
		<span>
		Der Sandwurm stürzt sich auf den Justifier mit dem stärksten elektrostatischem Feld (Körperschutz mit 
		Energie +X, Waffen mit Besonderheit Energie, ggf. Ausrüstung). Bei Nutzung eines Fahrzeugs, wird das ganze
		Fahrzeug angegriffen auch wenn sich kein Justifier darin befindet. Werte des Fahrzeugs sind dem Erzähler 
		überlassen. Es gilt nach dem Angriff als
		</span> 
		<span	class="emphasize">stark beschädigt</span>
	</div>
	<div class="talent">
		<span class="header">Immun gegen Energiewaffen</span>
		<span>
		Die Haut des Sandwurms besitzt eine natürlichen Absorbtionsmechanismus für Energie, weshalb auch die 
		größte Energiewaffe nicht mal einen Kratzer verursacht
		</span>
	</div>
	<div class="talent">
		<span class="header">Nachhut</span>
		<span>Wenn der Wurm besiegt wird, erscheint umgehend der nächste.</span>
	</div>
</div>

### Regionswechsel

Es ist Absicht, dass die Justifiers die Region "zu Fuß" durchqueren müssen, um ins Zentrum zu kommen. Da das
Zentrum immer direkt die Region der Landungszone angrenzt, sollte das kein größeres Problem sein. Sollten 
die Justifiers die anderen Regionen erkunden wollen, bevor sie zum Zentrum gehen, müssen sie ein 
Luftfahrzeug nutzen und an der Küste entlang fliegen. 

### Wüste I

#### Regionsbeschreibung

<div class="readaloud">Die Region liegt viele Meter über dem Meeresspiegel. Das Meer scheint so weit weg
zu sein, dass sich nicht der kleinste Tropfen Wasser hierher verirrt. Die Luft hat eine seltsamen Geruch, den 
ihr nicht deuten könnt.</div> 

#### Regionsereignis (Permanent bis Erhalt der <a href="#wurmscheuche">Wurmscheuche</a>)

Tragen die Justifiers elektrische Gerätschaften mit sich oder befinden Sie sich in einem Fahrzeug, werden sie 
von <a href="#sandwürmer">Sandwürmern</a> angegriffen.

#### Regionserkundung

Die Regionserkundung ist erst nach Erhalt der <a href="#wurmscheuche">Wurmscheuche</a> möglich

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
		<span class="dice">Bodenfahrzeug Malus -3</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud">Ihr richtet die Sensoren in die Dunkelheit und auch eure Nasen.</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf SEELE + WAHRNEHMUNG [RIECHEN] (2)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">Ihr meint einen würzigen Geruch auszumachen und konfiguriert die Scanner
		entsprechend. Diese melden, dass die Luft und der Boden mit einer organische Substanz durchzogen ist, 
		die Ähnlichkeit mit halluzinogenen Drogen hat. Auch wenn Gauss das natürlich offiziell dementieren wird,
		wird das dem Konzern viel Geld bringen.</span><span class="emphasize">+2 MP</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">Bei der Menge an tierischen Ausdünstungen, die von dir und deinen Kollegen 
		ausgehen, habt ihr Euch wohl geirrt.</span>
 	</div>
</div>

### Wüste II

Falls die Justifiers bereits in Szene 1, den Kontinent umflogen haben und der Scan für Wüste IV erfolgreich war, 
wissen sie bereits, dass hier verschiedene wertvolle Ressourcen liegen und haben bereits einen Missionspunkt 
dafür erhalten. Ein erfolgreicher Regionsscan bringt dann nur 1 statt 2 weiteren Missionspunkte.

Das gleiche gilt für den Fall, dass die Karte bereits während der Orbitalexploration "aufgedeckt" wurde. 
Also falls bei der Scan-Herausforderung während der Oribitalexploration mindestens 16 Erfolge erzielt wurden 
(siehe Seite 241 BRW)

#### Regionsbeschreibung

<div class="readaloud">
Zu Eurer großen Überraschung findet ihr hier Sand. Wo ihr auch hinschaut, überall liegen nur diese kleinen, kratzigen 
Körnchen herum. Auch Eure Geduld mit diesem Kontinent beginnt zu erodieren, wie die Gesteine von denen diese stammen. 
</div>

#### Regionsereignis (Permanent bis Erhalt der <a href="#wurmscheuche">Wurmscheuche</a>)

Tragen die Justifiers elektrische Gerätschaften mit sich oder befinden Sie sich in einem Fahrzeug, werden sie 
von <a href="#sandwürmer">Sandwürmern</a> angegriffen.

#### Regionserkundung

Die Regionserkundung ist erst nach Erhalt der <a href="#wurmscheuche">Wurmscheuche</a> möglich

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Regionserkundung:</span>
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
		<span class="dice">Bodenfahrzeug Malus -3</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud">Genervt beginnt ihr die Scanner zu aktivieren und die Analysesoftware laufen zu 
lassen. Aber auch eure geologischen Kenntnisse melden sich in Eurem Hinterkopf.</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + WISSEN [GEOLOGIE] (1)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">Woah! Was ist das denn. In der Geologie App überschlagen sich die Meldungen. 
		Ihr seid auf mehr als reiche Vorkommen gleich mehrerer Rohstoffe gestoßen. Salpter, Kupfer, Silber 
		und GOLD! Das gibt einen fetten Bonus von Gauss</span>
		<p>
		<span class="emphasize">+2 MP</span><span> wenn die Justifiers in Szene 1 die Region noch nicht 
		erfolgreich vom Shuttle aus gescannt haben. Andernfalls</span><span class="emphasize"> +1 MP</span>
		</p>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		Scheinbar ist es mit euren Geologie Kenntnissen doch nicht so weit her. Vielleicht sollte hier mal ein Profi ran. 
		</span>
 	</div>
</div>

### Wüste III

#### Regionsbeschreibung

<div class="readaloud">
	Vor Euch erstreckt sich eine von Dünen durchzogene Sandwüste. Von hier sieht es wie ein Katzensprung bis zur 
	Anhöhe der Zentralregion aus. 
</div>

#### Regionsereignis (Permanent bis Erhalt der <a href="#wurmscheuche">Wurmscheuche</a>)

Tragen die Justifiers elektrische Gerätschaften mit sich oder befinden Sie sich in einem Fahrzeug, werden sie 
von <a href="#sandwürmer">Sandwürmern</a> angegriffen.

#### Regionserkundung

Die Regionserkundung ist erst nach Erhalt der <a href="#wurmscheuche">Wurmscheuche</a> möglich

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Regionserkundung:</span>
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
		<span class="dice">Bodenfahrzeug Malus -3</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud">
Ihr schaut Euch um, könnt aber aufgrund der Lichtverhältnisse keine Details erkennen. Deshalb habt ihr wenig Hoffnung 
dass die Sensoren hier irgendetwas finden.</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + COMPUTER (2)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">Die Sensoren melden einen großen Vorrat an bestem, natürlichen ... Sand. Naja, manchmal 
		stimmt halt der erste Eindruck.</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">So richtig könnt ihr Euch keinen Reim aus den Werten der Sensoren machen. Sollte hier in 
		diesem langweiligsten aller Gebiete, tatsächlich mehr zu finden sein als nur Sand? Euer Instikt sagt nein.</span>
		<p>
		Sollten die Justifiers nach einem ersten erfolglosen Scan ein zweites Mal hierher kommen und die Herausforderung dann
		bestanden werden, sagt ihr, dass die seltsamen	Messwerte durch verschmutzte Sensoren zustande gekommen sind. Der 
		Schrauber sollte sich die vielleicht mal anschauen. 
		</p>
 	</div>
</div>

Zugegebenermaßen, war das wirklich eine langweilige Region. Aber es kann auch nicht in jeder Region ein Ancient 
Artefakt liegen.

### Wüste IV

#### Regionsbeschreibung

<div class="readaloud">
Sand. Überall ist Sand. Auf dem Boden, in der Luft, in Eurer Rüstung und in Eurer Unterwäsche. Mehr scheint es 
auf diesem Kontinent wirklich nicht zu geben. 
</div>

#### Regionsereignis (Permanent bis Erhalt der <a href="#wurmscheuche">Wurmscheuche</a>)

Tragen die Justifiers elektrische Gerätschaften mit sich oder befinden Sie sich in einem Fahrzeug, werden sie 
von <a href="#sandwürmer">Sandwürmern</a> angegriffen.

#### Regionsereignis (einmalig nach Erhalt der <a href="#wurmscheuche">Wurmscheuche</a>)

<div class="readaloud">
Ihr beginnt damit die Scanner zu konfigurieren als wie aus dem nichts ein ein Sturm aufzieht. Ihr müsst 
zusehen, dass ihr alles schnell wieder eingepackt bekommt und Euch und die Ausrüstung in Sicherheit bringt bis
der Sturm vorüber ist.
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf KÖRPER + SURVIVAL [WÜSTE] (4)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
			Der Wind knallt die Sandkörner gegen Euch und Eure Ausrüstung aber ihr habt alles gut verbarrikadiert.
		</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
		<span class="readaloud">Ihr wisst jetzt wie "Standstrahlen" funktioniert und Eure Ausrüstung auch.</span>
		<p>Alle Justifiers verlieren 1 Punkt Ausdauer. Alle Fahrzeuge gelten als beschädigt.</p>
 	</div>
</div>

#### Regionserkundung

Die Regionserkundung ist erst nach Erhalt der <a href="#wurmscheuche">Wurmscheuche</a> möglich

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Regionserkundung:</span>
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
		<span class="dice">Bodenfahrzeug Malus -3</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud">Ihr habt nur kurze Zeit, die Sensoren zu kalibrieren bis sie wieder vom Sand 
verklebt werden.</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + COMPUTER (3)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">Der Sand enthält eine höhere Konzentration an Silizium als vergleichbbare 
		Sande.</span>
		<span class="emphasize">+1 MP</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">Gibt es etwas Langweiligeres als Sand.</span> 
 	</div>
</div>


### Wüste V

#### Regionsbeschreibung

<div class="readaloud">
	Die Wüste ist hier etwas weniger sandig und im Hintergrund erhebt sich ein Gebirge, wie ihr es auf diesem 
	Kontintent gar nicht vermutet habt. 
</div>

#### Regionsereignis (Permanent bis Erhalt der <a href="#wurmscheuche">Wurmscheuche</a>)

Tragen die Justifiers elektrische Gerätschaften mit sich oder befinden Sie sich in einem Fahrzeug, werden sie 
von <a href="#sandwürmer">Sandwürmern</a> angegriffen.

#### Regionserkundung

Die Regionserkundung ist erst nach Erhalt der <a href="#wurmscheuche">Wurmscheuche</a> möglich

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Regionserkundung:</span>
    <span class="dice">Herausforderung auf GEIST + PILOT</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
		<span class="dice">Bodenfahrzeug Malus -3</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud">Die Luft ist ruhig und klar, als ihr beginnt die Gegend zu untersuchen. Hier und da 
findet ihr sogar ein wenig Vegetation, die jetzt in der kühlen Nacht das bisschen kondendisierte Wasser 
aufnimmt.</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + WISSEN [BIOLOGIE] (1)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<p class="readaloud">Die Pflanzen die hier wachsen sehen klein aus sind aber eigentlich riesig. Die 
		enorm tiefen Wurzeln und ein extreme effiziente Wasserspeicherung machen diese Pflanzen interessant
		für die Eierköpfe im Labor bei Gauss.</p>
		<p><span class="emphasize">+1 MP</span></p>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		Ihr findet nur halbtotes Gestrüpp. Wie soll in dieser Umgebung auch irgendetwas pflanzliches gedeihen.
		</span>
 	</div>
</div>

### Wüste VI

#### Regionsbeschreibung

<div class="readaloud">
	Wie fast nicht anders zu erwarten befindet ihr Euch umgeben von Sand. Sand soweit das Auge reicht. Obwohl
	ihr meint, in der Entfernung ein Gebirge zu sehen. Es scheint sehr weit weg zu sein aber ihr könnt die
	Distanz kaum schätzen.
</div>

#### Regionsereignis (Permanent bis Erhalt der <a href="#wurmscheuche">Wurmscheuche</a>)

Tragen die Justifiers elektrische Gerätschaften mit sich oder befinden Sie sich in einem Fahrzeug, werden sie 
von <a href="#sandwürmer">Sandwürmern</a> angegriffen.

#### Regionserkundung

Die Regionserkundung ist erst nach Erhalt der <a href="#wurmscheuche">Wurmscheuche</a> möglich

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Regionserkundung:</span>
    <span class="dice">Herausforderung auf GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
		<span class="dice">Bodenfahrzeug Malus -3</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud">Ihr konzentriert Euch auf die Umgebung um herauszufinden, was diese optischen 
Täuschungen verursacht. Das sagen Euch auch die Sensoren. Die Luft scheint eine Substanz zu enthalten, 
die sich negativ auf eure Wahrnehmung auswirkt.</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + REFLEXE </span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">Ihr schafft es gerade noch rechtzeitig die Helme aufzusetzen und die 
		Atemfiltereinstellungen zu korrigieren.</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		Mist ihr kommt nicht an die Helme ran, am besten ihr lasst Euch im Shuttle vom Doc nochmal durchchecken.		
		In einem Kampf wäre Eurer aktueller Zustand lebensgefährlich.
		</span>
		<span>Die Justifier erleiden einen Malus von -1 auf alle Herausforderungen auf</span>
		<span class="emphasize">GEIST + WAHRNEHMUNG</span><span> sowie bei <span class="emphasize">Angriffsaktionen</span> im Kampf</span>
 	</div>
</div>

### Wüste VII: Zentrum

#### Vor dem erste Eintreffen

Die Justifiers müssen die Wüstenregion, in welcher sich das Shuttle befindet, in der Nacht und zu Fuß 
durchqueren. Ein Fahrzeug würde in jedem Fall (Tag oder Nacht) die <a href="#sandwürmer">Sandwürmer</a> auf 
den Plan rufen, die die Justifiers unbarmherzig angreifen. Tagsüber können die Justifiers aufgrund der 
Temperaturen aber nicht ohne Fahrzeug überleben.

Während der Durchquerung dürfen die Justifiers keine elektronischen Geräte benutzen. Sie dürfen sie zwar mit
sich führen, aber sie müssen abgeschaltet sein. Das schließt auch die Verwendung schwerer Rüstungen aus, 
welche mit purer Muskelkraft nicht zu bewegen sind.

<div class="readaloud">
	<p>Ihr schleppt Euch und Eure Ausrüstung durch die nächtliche Wüste. Das Vorankommen ist aufgrund des 
	sandigen Untergrundes beschwerlich und die Kälte macht es zusätzlich ungemütlich.</p>
	<p>Ab und zu scheint sich der Sand unter euren Füßen ein wenig zu bewegen und ihr rechnet jeden Moment
	mit einem Wurmangriff. Das eure deaktivierten Waffen im Moment nur hochtechnisierter Ballast sind, trägt auch nicht 
	zur Beruhigung bei.</p>
	<p>Je näher ihr dem Gebirge in der Mitte des Kontinents kommt, desto stärker wird das Signal der Energiequelle
	und deren seltsamen Frequenz. Ihr habt so ein seltsames Bauchgefühl könnt aber nicht genau sagen was es ist.</p>
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf SEELE + INTUTITION (3)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">Euer Gefühl sagt Euch, dass die Aktiväten der Würmer unter dem Sand weniger werden,
		 je näher	ihr der Signalquelle kommt.</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		Ihr habt wohl nur etwas Falsches gegegessen. 
		</span>
 	</div>
</div>

#### Regionsbeschreibung

Die Justifiers können ihre Waffen und Ausrüstung wieder aktivieren, sobald sie die Region betreten haben, da 
die Würmer hier im Gebirge nicht angreifen können. Aber das müssen Sie ihnen ja noch nicht gleich aufs 
Butterbrot schmieren. 

<div class="readaloud">
	Das Zentrum der Wüste ist ein sehr hohes Gebirge, dessen Flanken zu allen Seiten steil abfallen. Aber
	ihr könnt an einigen Stellen Schluchten und Pässe erkennen, die vielleicht vor langer Zeit durch Flüsse und
	Bäche in den Fels geschnitten wurden.
</div>

Die Justifiers können die alten Bachläufe nutzen um weiter in das Gebirge aufzusteigen. Beim ersten Besuch 
werden Sie das notgedrungen zu Fuß machen müssen. Später können sie auch Fahrzeuge verwenden.

#### Regionserkundung

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Ohne Fahrzeug, Sammelherausforderung auf </span>
		<span class="dice">KÖRPER + ATHLETIK [KLETTERN] (Anzahl Justifiers * 3)</span>
  </div>
	<p style="padding: 0 40px 0 40px">
		Die Justifiers, welche in einer Runde keinen Erfolg erreichen, verlieren einen Punkt Ausdauer. Sind 
		alle Justifiers bewusstlos, müssen sie eine Missionsrunde aussetzen um einen Ausdauerpunkt zu regenerieren
		und können dann fortfahren.
	</p>
  <div class="challenge">
    <span class="role">Mit Fahrzeug, Herausforderung auf </span>
		<span class="dice">GEIST + PILOT (2)</span>
		<span class="dice">Handling des Erkundungsfahrzeugs hilft.</span>
  </div>
</div>

#### Regionsscan

<div class="readaloud">
Also eines steht fest: Die Kultur, die diese Gebäude errichtet hat, verstand etwas von Architektur und 
Anlagenbau. Aber der Zweck erschließt sich nicht durch bloßes Ansehen. 
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf GEIST + TECHNIK [ANLAGEN] (4)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<p class="readaloud">
		Die Anlage ist in konzentrischen Kreisen organisiert, welche den großen <a href="#szene-2-kuppelbau">Kuppelbau</a> 
		in der Mitte umschließen. In diesem Gebäude scheint auch die Energiequelle untergebracht zu sein, 
		deren Messwerte Euch hierher geführt haben.
		</p>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<p class="readaloud">
		Ihr könnt keine wirkliche Ordnung in dem Sammelsurium verschiedenster Gebäude ausmachen. Nur ein großer 
		Kuppelbau sticht Euch ins Auge. Aber kommt ihr dahin?
		</p>
		<span>Die Justifiers verlieren eine Missionsrunde wenn sie sich auf den Weg zum 
		<a href="#szene-2-kuppelbau">Kuppelbau</a> machen</span>
 	</div>
</div>

## Szene 2: Kuppelbau

### Was hier passiert

Die Justifiers müssen sich Zugang zum Kuppelbau im Zentrum der Anlage verschaffen. Das ist nicht so einfach,
weil dies der letzte Kampfplatz zwischen den Baton'Fa und den Fa'Baton war. Die Gruppe findet heraus, wie es zur 
Verwüstung des Kontinents kam. Sie werden Neues über den Grund des Krieges herausfinden und darüber warum die 
beiden Völker heute nicht mehr existieren.

Außerdem können sie ein Gerät finden, welches ihnen die Würmer vom Hals hält.

<div class="readaloud">
Euer Ziel ist klar zu erkennen, der Weg dahin allerdings nicht. Die  meisten Straße sind verbarrikadiert 
oder völlig zerstört. Eines ist klar, hier hat vor langer Zeit ein Kampf stattgefunden und dabei sind 
große Teile der Infrastruktur zerstört worden. Ihr müsst ständig die Richtung ändern, weil der eingeschlagene
Weg unpassierbar ist. 
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="dice">Herausforderung auf SEELE + WAHRNEHMUNG (2)</span>
  </div>
 	<div class="success">
   	<span>Erfolg</span>
		<span class="readaloud">
		Es ist ein ganz schönes Hin und Her aber schließlich biegt ihr in eine Straße ein, an deren Ende ihr ein
		Eingangstor zum Kuppelbau erkennen könnt. Aber auf Eurem Weg hierher konntet ihr eine sehr detailierte
		Karte der Anlage erstellen, die es dem Nachschubteam einfacher machen wird sich hier genauer umzusehen.
		</span>
		<span class="emphasize">+1 MP</span>
  </div>
 	<div class="failure">
   	<span>Misserfolg</span>
   	<span class="readaloud">
		Nach einer endlosen Tortur kreuz und quer durch die Anlage kommt ihr immer näher an den Kuppelbau heran.
		Ihr seid froh endlich am Ziel zu sein und verschwendet erstmal keinen Gedanken daran, dass ihr hier auch 
		wieder weg müsst. 
		</span>
 	</div>
</div>

### Sesam öffne dich

<div class="readaloud">
<p>Das Tor zum Kuppelbau unterscheidet sich nur farblich vom Rest der Mauer. Es sind keine Scharniere oder 
andere Öffnungsmechanismen auf den ersten Blick zu erkennen. Ebenso findet sich kein Schlüsselloch und auch 
kein Codeschloss mit dem sich die Verriegelung lösen lassen würde. Im Prinzip ist das Tor einfach nur ein 
massiver Block Metall.</p>
</div>

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Sammelstufenherausforderung auf verschiedene Fertigkeiten</span>
    <span class="dice">Runde 1 + 2: SEELE + INTUITION</span>
    <span class="dice">Runde 3 + 4: SEELE + WAHRNEHMUNG</span>
    <span class="dice">Runde 5 + 6: GEIST + TECHNIK [ANLAGEN]</span>
  </div>
		<div class="collect-challenge-result">
		<p>Falls die Justifiers es in den 6 Runden nicht schaffen, alle benötigten Erfolge zu sammeln, gilt 
		die Herausforderung insgesamt als fehlgeschlagen und sie können das Tor nicht öffnen. Fahren Sie dann 
		fort bei <a href="#fensterln">Fensterln</a></p>
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>5</span>
    	<span class="readaloud">
			Während ihr am Tor steht und Euch umseht, fallen Euch zwei Stelen, die sich in ca. 50 m 
			rechts und links des Tores befinden und mit der Mitte des Tores ein gleichseitiges Dreieck bilden. 
			</span>
  	</div>
		<div>
    	<span>10</span>
    	<span class="readaloud">
			Ihr werft einen genaueren Blick auf die Stelen und scannt sie mit allem was Euch zur Verfügung steht. 
			Schließlich bemerkt ihr etwas, das aussieht wie Bedienelemente, aber wenn ihr sie betätigt passiert 
			nichts. 
			</span>
		</div>
		<div>
    	<span>15+</span>
    	<p class="readaloud">
			Ja! Das muss es sein. Ihr findet einige zerstörte Energieleitungen und schafft es, die beschädigten
			Stellen zu überbrücken. Die Bedienelemente an den Stelen leuchten auf. Aber was macht man jetzt damit?
			</p>
			<p>Die Bedienelemente an jeder Stele haben nur dann eine Wirkung, wenn sie zeitgleich betätigt werden.
			Sobald vor jeder Stele mindestens ein Justifier steht, lesen Sie folgenden Text vor:
			</p>
			<p class="readaloud">
			Das muss es gewesen sein. Ihr seid Euch nicht ganz sicher, was ihr da gedrückt habt. Aber plötzlich 
			fängt das Tor an, sich zu bewegen. Ihr habt das Gefühl in den Schlund einer riesigen Kreatur
			zu schauen. 
			</p>
			<p>Weiter bei <a href="#in-der-kuppel">In der Kuppel</a></p>
		</div>
	</div>
</div>

### Fensterln

<div class="readaloud">
Egal was ihr versucht, die Tür bleibt einfach nur ein großer, metallischer Block in der Wand. Sie bewegt sich
kein Stück. Ihr braucht eine andere Möglichkeit in die Kuppel zu kommen.
</div>

Die Justifier müssen sich nun auf die Suche machen, nach einem anderen Eingang. Dieser existiert, in Form von Fenstern, 
die ganz oben auf der Kuppel sind und welche sie nur kletternd erreichen können. Es müssen aber nicht alle Justifiers gehen!

<div class="challenge-box">
  <div class="challenge">
		<span class="role">Sammelherausforderung für jeden kletternden Justifier einzeln</span>
    <span class="dice">KÖRPER + ATHLETIK [KLETTERN] (10)</span>
  </div>
	<div class="collect-challenge-result">
		<p>Kletternde Justifier, die in einer Runde keinen Erfolg würfeln, stürzen ab, verlieren einen 
		Ausdauerpunkt und müssen von vorn beginnen.</p>
 		<div class="success">
   		<span>Erfolg</span>
			<span class="readaloud">
			Ihr erreicht das Dach der Kuppel und ganz oben gibt es tatsächlich einen Ring aus Glas. Die Fenster sehen zwar sehr
			stabil, aber wahrscheinlich ist das eure beste wenn nicht gar letzte Option, um in den Kuppelbau zu kommen. 
			Warum ist eigentlich kein Glaser dabei, wenn man mal einen braucht.</span>
			<div class="challenge-box">
				<div class="challenge">
  				<span class="dice">Angriff mit der primären Waffe (2)</span>
    			<span class="dice">Alternativ Psi-Faust (2)</span>
    			<span class="role">Besondere Eigenschaften beachten</span>
  			</div>
 				<div class="success">
					<span>Erfolg</span>
					<span class="readaloud">Das Glas zerspringt und fällt in die Tiefe. Ihr lauscht kurz auf 
					irgendeine Reaktion, aber die kommt nicht. Ihr klettert in die Öffnung und seht Euch um. Ihr findet
					eine Treppe, die an der Außenwand der Kuppel hinunter führt. Am Boden angekommen findet ihr Euch 
					hinter dem Eingangstor wieder, aber hier befindet sich zum Glück ein Hebel neben der Tür ... ihr betätigt ihn 
					und das Tor gleitet auf.</span>
					<span>Weiter bei <a href="in-der-kuppel">In der Kuppel</a></span>
				</div>
 				<div class="failure">
					<span>Misserfolg</span>
					<p class="readaloud">Ihr rutscht ab und schlagt hart auf der Kuppeloberfläche auf, aber eine 
					herausstehendes Metallteil reißt Euch die Rüstung auf. Dafür rutscht ihr zumindest nicht 
					wieder bis zum Boden
					</p>
					<p>Die Justifier verlieren einen Ausdauerpunkt und einen Erfolg, dürfen aber weitermachen.
					Wenn sie bewustlos werden, fängt sie ein anderer Justifier auf. Sind alle Justifier
					bewusstlos, stürzen Sie ab und müssen erst eine Missionsrunde ausruhen.</p>
				</div>
			</div>
  	</div>
	</div>
</div>

### In der Kuppel

<div class="readaloud">
Ihr steht im Rahmen des Tors steht und starrt in den schlecht erleuchteten Gang dahinter. Rechts von Euch könnt ihr eine 
Treppe erkennen, die an der Außenwand der Kuppel nach oben führt. Linkerhand ist eine massive Wand und geradezu zwei Türen.
</div>

Lassen Sie die Justifiers entscheiden, wohin sie gehen wollen. Sie können sich auch aufteilen wenn sie das möchten. 
Erinnern Sie die Justifiers ggf. daran, dass sie sich über ihr JUST auch über Entfernungen miteinander unterhalten können. 
Für jeden Weg gibt es jeweils ein Unterkapitel, durch Sie die Spieler führen können. 

#### Treppe nach oben

Falls die Justifiers oder ein Teil der Justifiers nicht durch das Tor gekommen sind, sondern bereits über die Glasfenster von
oben, sollten sie hoffentlich erkannt haben, dass das die Treppe ist, über die sie nach unten kamen. Erinnern Sie die Justifiers 
andernfalls daran. 

Kamen die Justifiers alle durch das Eingangstor und gehen nun die Treppe hoch, lesen Sie folgenden Text vor. 
<div class="readaloud">
Ihr geht die sich windende Treppe bis nach oben und befindet Euch unter dem Dach der Kuppel, in das schwere Glasfenster eingelassen 
sind. Sie sind allerdings so verschmutzt, dass ihr kaum nach draußen blickt könnt. Hier scheint es nichts weiter interessantes zu geben. 
</div>

#### Der linke Gang

<div class="readaloud">
	<p>Ihr wählt die linke Tür und befürchtet schon, dass sich auch diese nicht ohne weiteres öffnen lässt. Doch als ihr näher 
	kommt, seht ihr eine	Art Türklinge und betätigt diese. Zu Eurer Überraschung öffnet sich die Tür und ihr tretet in den Gang 
	dahinter.</p>

	<p>Der Gang verläuft in einem Bogen parallel zur Außenwand. Das Ende könnt ihr auf Grund der Krümmung des Ganges nicht erkennen. 
	Aber dort scheint es eine schwache Lichtquelle zu geben, die in unregelmäigen Abständen stärker und schwächer wird.</p>

	<p>Zwischen der Außenwand und dem Gang sind Räume welche über große, massive Türen erreichbar sind. In die Türen sind kleine Fenster 
	eingelassen und daneben befinden sich Schilder in einer Schrift, die ihr nicht entziffern könnt.</p> 

</div>

Die Justifiers können nun entweder direkt weitergehen zum <a href="#am-ende-des-gangs">Ende des Gangs</a> oder sich zuerst die 
<a href="#labore">Labore</a> ansehen.

#### Labore

Die Labore sind entweder über <a href="#der-linke-gang">den linken Gang</a> oder über die <a href="#am-ende-des-gangs">Reaktorkammer
am Ende des Gangs</a> erreichbar.

Wenn die Justifiers durch eines der Fenster in den Türen schauen, lesen Sie folgenden Text vor.

<div class="readaloud">
Ihr schaut durch eines der Fenster und entdeckt einige Tische auf denen verschiedene Aufbauten von Rohrleitungen und Behälter aus Glas 
stehen. Zweifellos handelt es sich hier um ein Labor, aber von hier könnt ihr nicht sagen was Gegenstand der Forschung war. 
</div>

Sobald die Justifiers die Tür geöffnet haben:

<div class="readaloud">
Ihr öffnet die Tür und entdeckt neben Laborausrüstung in der Mitte des Raumes, weitere Tische an den Wänden auf denen sich rechteckige
Geräte befinden, die Euch an die Eingabegeräte alter Computer erinnern. Ein Blick unter die Tische scheint diese Vermutung zu bestätigen. 
Denn dort seht ihr graue Kisten, die wohl den eigentlichen Rechner darstellen. 
</div>

Die Justifiers können sich nun die Labore genauer ansehen, in dem sie sich der nachfolgenden Sammelstufenherausforderung stellen.

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Sammelstufenherausforderung auf verschiedene Fertigkeiten</span>
    <span class="dice">Runde 1 + 2: SEELE + INTUITION</span>
    <span class="dice">Runde 3 + 4: GEIST + COMPUTER </span>
    <span class="dice">Runde 5 + 6: GEIST + WISSEN [BIOLOGIE]</span>
  </div>
	<div class="collect-challenge-result">
		<p>
		Die Erfolge summieren sich in jeder Runde. Lesen Sie nach jeder Runde die Auswirkung des nächsten erreichten Erfolgsgrad vor.
		</p>
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>5</span>
    	<p class="readaloud">
			Verschiedene Apparate und Behältnisse erinnern Euch an den Biologie-Unterricht (Den Betas nur aus ihrer Progammierung
			im Natus-Tank kennen). Es stehen vereinzelt Petrischalen herum, in denen vermutlich irgendwann mal etwas gezüchtet wurde. 
			Andere Gerätschaften erinnern Euch vage an Mikroskope. An den Wänden hängen Tafeln mit Modellen und Zeichnungen von chemischen 
			Verbindungen.
			</p>
  	</div>
		<div>
    	<span>10</span>
    	<p class="readaloud">
			Auf der Suche nach einem Anschalter berührt ihr einige der Tasten auf dem Eingabegerät. Ihr wollt schon an einer anderen Stelle 
			weitersuchen, als plötzlich ein Holo-Display auf dem Tisch erscheint und euch ein Modell einer chemischen Verbindung anzeigt.
			Ihr prüft die Anzeige und schafft es schließlich über die Tastatur damit zu interagieren. Das sieht aus wie eine Auflistung 
			von Dateien. </p>
			<p>Falls die Justifier nicht bereits einen Computer aus den Büros mitgenommen haben, lesen Sie folgendes vor</p>
			<p class="readaloud">Ihr findet keine Möglichkeit die Daten zu übertragen, aber das könnte interessant sein. Kurzerhand nehmt ihr die Tastatur
			und die Kiste unter dem Tisch mit, um sie im Shuttle näher zu untersuchen. </p>
			<p>Die Justifier erhalten das Fundstück <a href="#computer-aus-dem-kuppelbau">Computer aus dem Kuppelbau</a> sowie 
			<span class="emphasize">+1 MP</span> </p>
			<p>Andernfalls lesen sie folgendes vor</p>
			<p class="readaloud">Das sieht alles den Computern in den Büros sehr ähnlich. Ihr lasst den Computer hier, ihr habt ja erstmal den anderen.</p>
		</div>
		<div>
    	<span>15+</span>
    	<p class="readaloud">
			Ihr erkennt in der chechmische Verbindung die vom Computer dargestellt wurde ein Protein und glaubt dass es sich vermutlich sogar um ein 
			Enzym handelt. Die Zeichnungen an den Tafeln erkennt ihr als DNA-Sequenzen. Ihr hegt den Verdacht, dass das Enzym zur Veränderung dieser 
			DNA-Sequenzen eingesetzt werden könnte und das die Labors und Gerätschaften zur Synthetisierung des Enzyms dienen könnten. Ihr müsst die 
			Daten auf diesen Computern analysieren um mehr zu erfahren. 
			</p>
			<p>
			Die Justifiers erhalten <span class="emphasize">+2 MP</span> sowie <span class="emphasize">+1 Geschichtspunkt</span>
			</p>
		</div>
	</div>
</div>

Falls die Justifiers sich die anderen Labore anschauen wollen, können Sie das gern tun, aber sie werden nichts Neues finden.
Sie sollten dann irgendwann zum <a href="#am-ende-des-gangs">Ende des Gangs</a> weitergehen.

#### Der rechte Gang

<div class="readaloud">
<p>
Ihr tretet durch die Tür und steht in einem dunklen Gang, dessen Ende von einer unrhythmisch pulsierenden Lichtquelle schwach erhellt wird. Ihr könnt 
die Lichtquelle aber aufgrund der Biegung des Gangs nicht erkennen.
</p>
<p>
Zu eurer Rechten befinden sich Räume, die durch Glasfronten vom Gang abgetrennt sind. Die Räume sind möbliert und ihr schätzt, dass es sich um Büros 
handelt. Es gibt Tische und Sitzmöglichkeiten. Auf den Tischen befinden sich altmodische Tastaturen und darunter graue Kisten. An den Wänden 
befinden sich Tafeln, die Ähnlichkeiten mit Whiteboards haben, die ihr aus Bildern aus den 2000er Jahren der Erde kennt.
</p>
<p>
Alle Whiteboards sind leer bis auf eines im ersten Büro. Darauf erkennt ihr eine Explosionszeichnung eines elektronischen Gerätes und daneben
eine schematische Darstellung eines Frequenzganges.
</p>
</div>

Die Justifiers können nun entweder direkt weitergehen zum <a href="#am-ende-des-gangs">Am Ende des Gangs</a> oder sich zuerst die 
<a href="#büros">Büros</a> ansehen.

#### Büros

Die Büros sind entweder über <a href="#der-rechte-gang">den rechten Gang</a> oder über die <a href="#am-ende-des-gangs">Reaktorkammer
am Ende des Gangs</a> erreichbar.

<p>Die Justifiers müssen sich folgenden Herausforderungen stellen, wenn Sie die Büros durchsuchen.</p>

<div class="challenge-box">
  <div class="challenge">
    <span class="role">Sammelstufenherausforderung auf verschiedene Fertigkeiten</span>
    <span class="dice">Runde 1 + 2: SEELE + INTUITION</span>
    <span class="dice">Runde 3 + 4: GEIST + COMPUTER </span>
    <span class="dice">Runde 5 + 6: GEIST + WISSEN [ELEKTRONIK]</span>
  </div>
	<div class="collect-challenge-result">
		<p>
		Die Erfolge summieren sich in jeder Runde. Lesen Sie nach jeder Runde die Auswirkung des nächsten erreichten Erfolgsgrad vor.
		</p>
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>5</span>
      <p class="readaloud">
					Auf einem der Schränke an der Wand erkennt ihr ein handkoffergroßes Geräts und auf einem Whiteboard daneben 
					die schematische Darstellung eines Frequenzganges. Das sieht interessant aus und ihr nehmt das Gerät einfach mit,
					um es später zu untersuchen. 
			</p>
      <p class="readaloud">
					Außerdem findet ihr anatomische Darstellungen von Echsen- und Vogelwesen. 
					Die Vögel ähneln entfernt den Elfern, sind aber wesentlich größer und verfügen über zusätzliche Arme unterhalb der Flügel, welche wiederum
					den Armen der Menschen recht ähnlich zu sein scheinen. Die Hände verfügen aber nur über 4 Finger entsprechen dem Aussehen der Füße, wenn 
					auch deutlich kleiner.
			</p>
			<p class="readaloud">
					Die Echsenwesen ähneln keiner Euch bekannten Art und ihr ordnet sie nur deshalb als Echse ein, da ihre Haut 
					mit Schuppen überzogen ist und sie einen länglichen Körper besitzen, den sie mittels vier krallenbewehrter
					Füße fortbewegen. 
 			</p>
			<p>
				Die Justifier erhalten das Fundstück <a href="#wurmscheuche">Wurmscheuche</a> welches sich aktiviert, sobald sie die Zone Wüste VII verlassen
				und in eine der anderen Wüsten gehen. Sie sind ab dann vor den Würmern sicher, die nun einen großen Bogen um das Gerät machen. 
				<span class="emphasize">Das wissen sie	aber noch nicht</span>
				Die Justifiers erhalten für diesen spektakulären Fund <span class="emphasize">+2 MP</span>.
				Die Justifiers erhalten weiterhin <span class="emphasize">+1 Geschichtspunkt</span>
			</p>
  	</div>
		<div>
    	<span>10</span>
    	<p class="readaloud">
			Ihr versucht die Computer unter den Tischen irgendwie anzuschalten und nach einigen Tastendrücken erscheint ein Holo-Display,
			welches scheinbar aus der Tastatur heraus erzeugt wird. In der Darstellung erkennt ihr die gleiche Explosionszeichnung wie auf dem
			Whiteboard. Aber es scheinen sich noch weitere Daten auf dem System zu befinden. Neben weiteren elektrotechnischen Zeichnungen findet
			ihr auch Darstellungen chemischer Verbindungen. 
			</p>
			<p>Falls die Justifier noch <span class="emphasize">keinen</span> Computer aus den Labors mitgenommen haben, lesen Sie folgendes vor:</p>
			<p class="readaloud">
			Ihr findet keine Möglichkeit die Daten zu übertragen, aber das könnte interessant sein. Kurzerhand nehmt ihr die Tastatur
			und die Kiste unter dem Tisch mit um sie im Shuttle näher zu untersuchen. 
			</p>
			<p>Die Justifier erhalten das Fundstück <a href="#computer-aus-dem-kuppelbau">Computer aus dem Kuppelbau</a> sowie 
			<span class="emphasize">+1 MP</span></p>
			<p>Andernfalls lesen sie folgendes vor</p>
			<p class="readaloud">Das sieht alles den Computern in den Büros sehr ähnlich. Ihr lasst den Computer hier, ihr habt ja erstmal den anderen.</p>
		</div>
		<div>
    	<span>15+</span>
    	<p class="readaloud">
			Ihr betrachtet den Fequenzgang an der Tafel nochmal genauer und plötzlich fällt euch etwas ein. Ihr ruft die Messwerte der Energiequelle in Eurem
			JUST auf, die ihr bei Eurer Ankunft auf dem Kontinent erfasst habt. Tatsächlich ... der Rhythmus, in der die Linien auf und ab gehen, scheint sehr
			ähnlich zu sein. Nur sind die Linien in der Zeichnung wesentlich flacher. Bei dem Gerät scheint es sich um einen Miniaturnachbau der 
			Energiequelle zu handeln zumindest scheint sie das gleiche Signal zu senden, wenn auch deutlich schwächer.
			</p>
			<p>
			Die Justifiers erhalten <span class="emphasize">+2 MP</span>
			</p>
		</div>
	</div>
</div>

Die Justifiers können auch die anderen Büros weiter untersuchen finden aber nichts Nennenswertes. Sie sollten dann zum 
<a href="#am-ende-des-gangs">Ende des Gangs</a> weitergehen.

#### Am Ende des Gangs

Auch wenn die Gruppe sich auf die beiden Gänge aufgeteilt hat, lesen Sie für die Ersten die hier eintreffen, den folgenden Text vor. Falls die 
Gruppe sich nicht aufgeteilt hatte, könnten sie jetzt auch erstmal zur anderen Seite des Gangs weitergehen.

<div class="readaloud">
<p>Ihr geht den Gang immer weiter bis die Wand neben Euch plötzlich endet. Als ihr noch weiter herum geht, steht ihr am Rand eines runden Raumes 
in dessen Mitte sich eine Vorrichtung erhebt, in deren Inneren ein bläuliches Licht pulsiert. Ihr habt wohl die Energiequelle 
gefunden. Die Anlage selbst ist schon eine Belohnung wert aber Eure geliebter Konzern lässt sicherlich noch ein paar Extras springen, 
wenn ihr herausfinden könnt, was damit betrieben wurde. 
</p>
<p>
Unweit eurer momentan Positionen befinden sich ein kleiner Raum mit einigen Steuerkonsolen. Diese sollte sich mal jemand ansehen, der
was davon versteht. Wer weiß was passiert, wenn man hier etwas falsches einstellt und die Energiequelle übersteuert. 
</p>
<p>
Ihr seht nun auch den Ausgang des anderen Gangs, der scheinbar einfach nur auf der anderen Seite um die Reaktorkammer herumgeführt hat.
</p>
</p>
</div>

Die Gruppe muss einen Justifier bestimmen, der in den Raum geht und die Anlage mit der nachfolgenden Herausforderung untersucht.
Direkt nach der ersten Würfelrunde lesen Sie den nachfolgenden Text vor. Die Justifiers außerhalb der Steuerungskabine müssen sich gegen
<span style="font-style:italic" class="emphasize">Anzahl Justifiers (gesamt)</span> <a href="#lasergeschuetz">Lasergeschützen</a> verteidigen und diese angreifen, 
bis der Kollege in der Kabine die Herausforderung bestanden hat. 

Falls noch Justifiers im anderen Gang sein sollten, wäre jetzt der Zeitpunkt diese per JUST dazu zu holen, so dass jeder 
Justifier von einem Geschütz angegriffen wird. 

Die Verteidigung gegen die Geschütze ist immer <span class="emphasize">GEIST + REFLEXE</span> gegen den Angriffswert des Geschützes.

<div class="readaloud">
<p>
Du beginnst die Anzeigen zu verstehen und beginnst, den ein oder anderen Schalter zu betätigen, um herauszufinden, was diese wohl bewirken. 
Die Tür zu dem kleinen Raum in dem du dich befindest schließt sich hinter dir und du bist hier gefangen. Sonst merkst du nichts, kein Gas, keine 
Selbstschussanlage oder Elektroschocks. Es scheint sich um eine Art Schutzmechanismus zu handeln, der den Controller vor Einwirkungen von außen schützt.
</p>
<p>
Deine Vermutung scheint sich zu bestätigen, als du siehst wie deine Kameraden plötzlich in alle Richtungen davon springen, weil sie von 
<a href="#lasergeschuetz">Lasergeschützen</a> beschossen werden, die sich an der Decke des Raumes befinden.
</p> 
</div>

<div class="challenge-box">
  <div class="challenge">
		<span class="role">Sammelherausforderung für den Justifier in der Controller-Kabine</span>
    <span class="dice">GEIST + TECHNIK [ANLAGEN] (15)</span>
  </div>
	<div class="collect-challenge-result">
 		<div class="success">
   		<span>Erfolg</span>
			<p class="readaloud">
			Jetzt hast du es raus. Das scheint der Knopf gewesen zu sein, der die Tür wieder öffnet und somit auch die Lasergeschütze abschaltet. 
			</p>
		</div>
	</div>
</div>

<div class="enemy">
	<div id="lasergeschuetz" class="header">Lasergeschütze</div>
	<ul>
		<li><span>Angriff</span>3/3/3 (Energie)</li>
		<li><span>Schaden</span>3</li>
		<li><span>Verteidigung</span>5</li>
		<li><span>Ausdauer</span>8</li>
		<li><span>Gefahr</span>4</li>
	</ul>
	<div>
		<span class="emphasize">Manöver</span><span>Jedes Geschütz attackiert einen anderen Justifier (gleichmäßige Verteilung). Sind 
		weniger Justifiers als Geschütze vorhanden, greifen die überzähligen Geschütze primär die Justifiers mit der stärksten Rüstung an.
		</span>
	</div>
	<div class="talent">
		<span class="header">Explosion</span><span>Wenn ein Geschütz zerstört wird, werden alle umstehenden Justifiers von Schrapnellen getroffen,
		wenn sie keine <span class="emphasize">Herausforderung auf GEIST + REFLEXE</span> bestehen und verlieren deshalb 1 Ausdauerpunkt</span>
	</div>
</div>

Sobald der Justifier die benötigten Erfolge erzielt hat, hört der Beschuss durch die Geschütze auf (falls noch welche vorhanden sind) und der
Justifier kann die Kabine verlassen. Falls alle anderen Justifiers bewusstlos sind, kann der Justifier versuchen, die anderen Gruppenmitglieder 
zu verarzten (<span class="emphasize">Painkillerpatch, Spezialgebiet Wissen: Medizin, etc.</span>). Verfügt er über kein solches Wissen oder Ausrüstung, 
verliert die Gruppe eine Missionsrunde. 

<div class="readaloud">
<p>Aufgeregt erzählst du, was du in der Kabine herausgefunden hast, nämlich dass die Anlage der Energieversorgung eines Terraforming-Moduls diente. 
Das Modul diente dazu den Kontinent in ein bewohnbares Klima zu verwandeln, wurde aber scheinbar in der Vergangenheit beschädigt, was zur Verwüstung
des Kontinents führte.</p>
</div>

Falls die Justifiers noch keines der anderen Terraforming-Module gefunden haben, lesen Sie außerdem den folgenden Text vor:
<div class="readaloud">
<p>Als die anderen Teammitglieder nicht die erhoffte Reaktion zeigen, fängst du an über beide Backen zu grinsen und fügst hinzu: 
"Das Modul ist ein Ancient-Artekfakt". Daraufhin macht sich allgemeine Freude breit. Denn das heißt, dass ihr dafür eine fürstliche Belohnung 
bekommen werdet.</p>
<p>Du führst außerdem weiter aus, dass du glaubst dass nur der Kontinent in Zone 1 von Natur aus ein Klima hatten, in dem die mittlerweile 
ausgestorbenen Rassen überleben konnten. Das wiederum bedeutet, dass in Zone 3 und 4 wohl auch noch Terraforming Module zu finden
sein sollten. Und die funktionieren wahrscheinlich noch, wenn ihr euch die Bilder aus dem Orbit in Erinnerung ruft.</p>
</div>

Die Justifiers erhalten <span class="emphasize">+10 MP</span>.

Sobald sich die Justifiers auf den Rückweg zum Shuttle machen wollen, lesen sie folgenden Text vor:

<div class="readaloud">
<p>Ihr macht Euch an den Abstieg aus dem Gebirge in Richtung Wüste. Der Weg nach unten ist fast noch anstrengender als der Weg nach oben. Aber 
schlussendlich kommt ihr am Fuß des Gebirges an, als bereits die Sonne aufgeht. Bei Tag schafft ihr die Durchquerung des Wüstenabschnitts auf keinen 
Fall. So macht ihr Euch also auf die Suche nach einer Möglichkeit sich vor der Sonne zu verstecken und auf den Einbruch der Nacht zu warten.
</p>
<p>
Ihr findet nach kurzer Zeit eine kleine Höhle, die ausreichend weit ins Innere des Felsmassivs führt, um Euch vor der Hitze zu schützen. Ihr sucht Euch
eine gemütliches Plätzchen und beginnt zu warten. Aber der Tag ist recht lang, so dass ihr Euch vielleicht etwas Beschäftigung suchen solltet.
</p>
</div>

Die Justifiers haben jetzt die Möglichkeit die folgenden Explorationshandlungen auszuführen: <span class="emphasize">Reparieren, Untersuchungen, 
Erholen, Freizeit, Nichts tun.</span>. Für die Untersuchungen steht kein Labor zur Verfügung aber es wäre sicherlich gut, das gefundene Gerät mal 
genauer unter die Lupe zu nehmen. Für die Analyse des mitgenommenen Computers allerdings braucht es unbedingt das Labor, so dass die Justifiers hier
keine neuen Erkenntnisse erhalten können bevor sie wieder am Shuttle sind.

## Abreise oder Spielende

Die Justifiers müssen das Shuttle reparieren und aus dem Sand befreien bevor sie abreisen können. 
Wenn die Justifiers entscheiden, den Kontinent zu verlassen lesen bitte den folgenden Text vor:

<div class="readaloud">
<p>Als ihr das Shuttle endlich aus dem Sand befreit und repariert habt, hofft ihr inständig, dass der Antrieb 
noch funktioniert und es schafft das Shuttle aus der Grube zu holen. Ihr habt einfach keinen Bock mehr
auf diesen Kontinent. Der Sand steckt überall und diese ewige Hitze macht euch und eurer Ausrüstung sehr 
zu schaffen. Was gäbt ihr jetzt für eine Dusche. Eine lange, kalte Dusche. </p>
<p>
Als Euer Pilot den Antrieb startet, rattert es kurz und anschließend verstummt der Antrieb wieder. Wild 
fluchend rennt der/die Techniker/in zur/zum Pilotin/Piloten und beschimpft sie/ihn aufs Übelste. Beide wagen einen
neuen Versuch und diesmal scheint der Antrieb tatsächlich die Startsequenz zu überleben und das Shuttle 
beginnt, sich langsam zu heben.
</p>
<p>
"Anschnallen Leute, das wird jetzt hart" hört ihr aus dem Cockpit als der Antrieb aufheult und das Shuttle 
auf Voll-Speed beschleunigt. Nachdem ihr die Grube verlassen habt und alle an Bord hörbar aufatmen, erklärt
der Pilot, dass es schnell gehen musste, damit der Sand nicht zu stark verwirbelt wird. Das hätte der Antrieb
nicht verkraftet.
</p>
<p>
Als ihr zähneknirschend darüber nachdenkt dem Piloten mal den Kopf gerade zu rücken, spürt ihr wieder den 
Sand im Mund. Hauptsache weg von hier, denkt ihr und schaut auf das Meer unter Euch, über das ihr auf den 
nächsten Kontinent zufliegt.
</p>
</div>

Falls die Missionsrunden zu Ende sind, während die Justifiers noch in Zone 2 unterwegs sind und das Shuttle 
entsprechend noch nicht freigelegt wurde, lesen Sie folgendes vor.

<div class="readaloud">
Plötzlich meldet sich euer JUST mit der Warnung, dass das Portal aktiviert wurde und das Nachschub-Team in 
wenigen Stunden eintrifft. Ihr müsst Euch umgehend zurück zum Shuttle begeben.
</div>

Falls die Justifiers bereits am Shuttle sind wenn alle Missionsrunden verbraucht wurden oder nach dem Vorlesen
obigen Texts, fahren sie mit folgendem Text fort:

<div class="readaloud">
<p>
Das Portal im Shuttle springt an und das Nachschub-Team tritt hindurch. Die Anführerin des Teams schaut 
sich kurz um und brüllt Euch an, was zur Hölle hier los sei und warum das Shuttle hier im Sand begraben ist. 
Ihr gebt einen kurzen Bericht ab wie ihr in diese missliche Lage geraten sein und bekommt umgehend den Befehl 
eure Sachen zu packen und euch anschließend im Besprechungsraum des Shuttles einzufinden.
</p>
<p>Fahren sie fort mit <a href="#abschlussbriefing">Abschlussbriefing</a></p>
</div>

## Fundstücke

### Computer aus dem Kuppelbau 

Für die Analyse des Computers ist unbedingt ein Labor im Shuttle notwendig.

<div class="challenge-box">
	<p>Lesen Sie alle Ergebnisse bis zur höchsten erreichten Zahl an Erfolgen vor. Erfolge aus vorherigen Versuchen werden hinzuaddiert</p>
  <div class="challenge">
    <span class="role">Sammelstufenherausforderung auf mehrere Fertigkeiten</span>
    <span class="dice">Die ersten vier Erfolge: GEIST + COMPUTER</span>
    <span class="dice">Für alle nachfolgenden Erfolge: GEIST + WISSEN [ETHNOLOGIE]</span>
  </div>
	<div class="collect-challenge-result">
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>3</span>
      <p class="readaloud">
			Ihr schafft es den Computer im Labor in Betrieb zu nehmen. Erste Analysen der Hardware deuten auf eine vollkommen unbekannte Struktur hin. 
			Aber je länger ihr Euch damit beschäftigt desto mehr versteht ihr die Funktionsweise und könnt auch Rückschlüsse auf die Arbeitsweise der 
			Software ziehen. So langsam versteht ihr das Design und wie man mit dem System interagiert. Aber ihr versteht einfach die Schriftzeichen nicht.	
			</p>
			<p>Die Justifier erhalten <span class="emphasize">+1 MP</span></p>
  	</div>
  	<div>
    	<span>6</span>
      <p class="readaloud">
			Ihr bekommt ein Gefühl für die Schriftzeichen und könnt wiederkehrende Muster erkennen, die es Euch erlauben die Navigation in dem 
			fremden Computersystem nachzuvollziehen. Ihr findet jetzt weitere Daten die ihr analysieren könnt. Mithilfe eures Verständnisses der Hardware 
			schafft ihr es eine Schnittstelle zwischen den Bordsystemen und dem gefundenen Computer aufzubauen, so dass ihr die Daten nun auf vertrauten
			System genauer unter die Lupe nehmen könnt.
			</p>
			<p>Die Justifier erhalten <span class="emphasize">+1 MP</span></p>
  	</div>
  	<div>
    	<span>8+</span>
      <p class="readaloud">
			Heureka! Neben den Modellen chemischer Verbindungen und elektronischer Bauteile	findet ihr verschiedene Texte zur Historie des Planeten und der
			ausgestorbenen Rassen der Baton'Fa und Fa'Baton.
			</p>
			<p>Die Justifier erhalten <span class="emphasize">+1 MP</span> sowie <span class="emphasize">+1 Geschichtspunkt</span></p>
  	</div>
	</div>
</div>

### Wurmscheuche

<div class="challenge-box">
	<p>Lesen Sie alle Ergebnisse bis zur höchsten erreichten Zahl an Erfolgen vor. Erfolge aus vorherigen Versuchen werden hinzuaddiert</p>
  <div class="challenge">
    <span class="role">Sammelstufenherausforderung auf mehrere Fertigkeiten</span>
    <span class="dice">Runde 1 + 2: GEIST + WISSEN [ELEKTRONIK]</span>
    <span class="dice">Runde 3 + 4: GEIST + WISSEN [BIOLOGIE]</span>
  </div>
	<div class="collect-challenge-result">
  	<div class="header"><span>Erfolge</span><span>Auswirkung</span></div>
  	<div>
    	<span>1</span>
      <span class="readaloud">
			Ihr untersucht das Gerät und findet eine Art Schalter über die ihr das Gerät vermutlich einschalten könnt.
			</span>
  	</div>
  	<div>
    	<span>2</span>
      <span class="readaloud">
			Ihr betätigt den Schalter und setzt die Scanner in eurem JUST auf das Gerät an. Deren Ergebnisse sagen aus, dass das Gerät zwar Energie
			erzeugt, aber gerade so viel, dass es sich selbst am Leben halten kann. Dabei strahlt es die gleiche Signatur aus, wie die Anlage im Kuppelbau
			auf dem Berg von Zone 2.
			</span>
  	</div>
  	<div>
    	<span>4+</span>
      <p class="readaloud">
			Eure Scanner nehmen plötzlich weitere Signale aus der Umgebung auf, die denen des Gerätes sehr ähnlich aber in der Phase verschoben sind. 
			Dieses zusätzliche Signal scheint schwächer zu werden und ihr verstärkt euer eigenes Signal. Kurz darauf und ohne Vorwarnung 
			schießt ein Sandwurm aus dem Boden, in unmittelbarer Entfernung zu Eurer aktuellen Position. 
			</p>
      <p class="readaloud">
			Doch statt anzugreifen bewegt sich der Sandwurm von Euch weg, weiter in die Wüste hinein. Egal wofür es gedacht war, das Gerät scheint 
			Euch die Würmer vom Hals zu halten. In Ermangelung jedweder Fantasie tauft ihr es "Wurmscheuche".
			</p>
			<p>Die Justifiers erhalten einen Missionsbonus von <span class="emphasize">+2 MP</span></p>
  	</div>
	</div>
</div>


## Notizen
* Die J finden eine alte Anlage eines der vergessenen Stämme/Völker
* Darin finden die J ein Gerät, welches ihnen die Würmer vom Hals hält
* Der Kuppelbau: 
  *  Zentrale Forschungseinrichtung der Fa'Baton (Vögel) in der sie versucht haben das Enzym zu synthetisieren, dass ihren Alterungsprozess verlangsamt. 
			Außerdem enthält es einen Reaktor, der das Terraforming Modul für Zone 2 betrieb und außerdem Energie für die Forschung der Fa'baton
			lieferte
  * Baton'Fa erfuhren von der Forschung und bekämpften die Fa'Baton und drängten sie in Zone 2 zurück. Schlussendlich zerstörten sie die Fa'Baton
	  in dem sie sie in Zone 2 isolierten und das Terraforming-Modul beschädigten. Dadruch starben die Fa'Baton aus (kein Enzym => kurze Lebenszeit). 
		Da die Baton'Fa aber so nicht mehr genug Botenstoff bekamen, starben Sie langsam aus, da sie nicht mehr genug Nachkommen produzierten.

* Die ausgestorbenen Rasse:
  * Baton'Fa: vogelartige Wesen
  * Fa'Baton: echsenartige Wesen
  * Baton'Fa benötigen für die Fortpflanzung einen Botenstoff der Fa'Baton. Ohne diesen ist die Fortplanzungsrate zu gering 
	(zu wenig Nachkommen um Rasse zu erhalten)
  * Eier der Baton'Fa enthalten ein Enzym, welches die Fa'Baton für den Erhalt ihrer Körper (Verzögerung Altersprozess)
	 benötigen. Ohne das Enzym leben die Fa'Baton nur kurze Zeit und können nicht genug nachkommen produzieren; Rasse stirbt aus
  *  Symbiose: 
    * Baton'Fa geben Eier an die Fa'Baton ab (früher geraubt von Fa'Baton), erhalten dafür Botenstoff (früher am Nest hinterlassen 
			während des Raubes), der dafür sorgt, dass mehr Eier gelegt werden 
      *  Im Ergebnis bleiben mehr für die Baton'Fa übrig, da insgesamt mehr Eier produziert werden können 
         => Mehr Nachkommen => Bevölkerungswachstum
		* Fa'Baton ernähren sich von den Eiern (u.a.) und werden älter
      * können mehr Nachkommen zeugen => Bevölkerungswachstum

# Abschlussbriefing

<div class="TODO">Abschlussbriefing</div>

* Stadt der Götter muss besucht worden sein, sonst hätten die Justifier Zone 1 nicht verlassen können
