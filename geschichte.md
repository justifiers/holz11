---
layout: mission
title: 'Geschichte'
---

# Geschichte des Planeten

<div id="wip"/>

Die Justifiers können während der gesamten Mission <span class="emphasize">Geschichtspunkte</span> sammeln. Lesen sie dann jeweils aus der folgenden
Tabelle bis zur Anzahl der bisher erreichten Geschichtspunkte vor.

<div class="challenge-box">
	<div class="collect-challenge-result">
  	<div class="header"><span>Geschichtspunkte</span><span>Vorlesetext</span></div>
  	<div>
    	<span>1</span>
      <span class="readaloud">
			Lange vor den Elfern scheinen zwei Rassen die vorherrschende Spezies gewesen zu sein. Auf der einen Seite die Baton'Fa; vogelartige Wesen, 
			die neben Flügeln auch über Arme und Hände verfügten. Daneben existierten die Fa'Baton die eher den irdischen Echsen ähnelten, aber wesentlich
			größer waren.
			</span>
  	</div>
  	<div>
    	<span>2</span>
      <span class="readaloud">
			Die Fa'Baton und die Baton'Fa scheinen kurz vor dem Aussterben der beiden Rassen in einem Krieg miteinander gelegen zu haben. Die Ursache könnt ihr
			noch nicht genau ermitteln aber ihr findet heraus, dass die letzte Schlacht wohl auf dem Berg von Zone 2 stattfand und mit der 
			Niederlage der Fa'Baton geendet haben muss. Ursächlich dafür war wohl die Zerstörung eines Terraforming-Moduls in Zone 2. Das Terraforming-Modul 
			stammte nicht aus den Schmieden der beiden Rassen, sondern befindet sich schon länger auf dem Planeten und schaffete überhaupt erst die 
			Möglichkeit für die Entstehung von intelligentem Leben.
			</span>
  	</div>
  	<div>
    	<span>3</span>
      <span class="readaloud">
			Der Kuppelbau auf dem Berg von Zone 2 gehörte den Fa'Baton und war sowohl die Energiequelle für das Terraforming-Modul dort, als auch für 
			eine Forschungseinrichtung, die das Ziel hatte ein Enzym zu synthetisieren, welches zur Verlängerung des Lebens der Fa'Baton dienen sollte. 
			Es scheint nur ein natürliches Vorkommen des Enzyms auf dem Planeten gegeben zu haben: Die Eier der Baton'Fa. Ohne dieses Enzym war die 
			Lebenserwartung der Fa'Baton wohl so gering, dass sie nicht genügend Nachkommen zeugen konnten um das Überleben der Rasse zu sichern. 
			</span>
  	</div>
  	<div>
    	<span>4</span>
      <span class="readaloud">
			In der Evolution der beiden Rassen spielte das Enzym, über welches der finale Krieg entstand, eine wichtige Rolle. Zusammen mit einem 
			Botenstoff, den nur die Fa'Baton natürlich erzeugen konnten, legte das Enzym die Grundlage für eine symbiotische Beziehung zwischen den beiden
			Rassen. Ursprünglich stahlen die Fa'Baton die Eier der Baton'Fa aus deren Nestern. Dabei hinterließen sie einen Botenstoff der die Fruchtbarkeit
			der Baton'Fa drastisch erhöhte, was dazu führte das mehr Eier gelegt wurden und somit mehr Baton'Fa schlüpfen konnten. Die Eier wiederum 
			erlaubten den Fa'Baton ein längeres Leben und somit die Zeugung weiterer Nachkommen, was sich ebenfalls in einer erhöhten Population niederschlug.
			</span>
  	</div>
  	<div>
    	<span>5</span>
      <span class="readaloud">
			Die Symbiose der beiden Rassen erlaubte es ihnen, zu den beherrschenden Spezies des Planeten zu werden und schließlich fortschrittliche 
			Zivilisationen hervorzubringen. Der bewusste Austausch der Eier gegen den Botenstoff begann, führte aber zu enormen politischen Schwierigkeiten.
			Die Geschichte der beiden Völker war durchwachsen von verschiedenen Kriegen und politischen Intrigen, aber da das Überleben der einen nicht 
			ohne das Überleben der anderen ging, folgte auf jeden Krieg ein erzwungener Frieden.
			</span>
  	</div>
  	<div>
    	<span>6</span>
      <span class="readaloud">
			Schlussendlich gelang den Forschern der Fa'Baton ein Durchbruch in Bezug auf die Erforschung des Enzyms aus den Baton'Fa Eiern. Sie standen kurz 
			davor sich von der Rasse zu emanzipieren, die sich im letzten Krieg einen deutlichen Vorteil erkämpft hatte. Aber die Baton'Fa erfuhren von 
			den Bemühungen und trieben die Fa'Baton bis auf ihre letzte Bastion im Herzen von Zone 2 zurück. Sie zerstörten das Terraformning-Modul und 
			isolierten die Fa'Baton auf dem Berg. Diese hatten keine Ressourcen mehr um die Enzym-Synthetisierung fertig zu stellen und waren auch durch 
			die Isolation von jeder natürlichen Enzymquelle getrennt. In einer letzten Aktion der Rache stürzten sich die letzten Fa'Baton in die Schluchten
			des Gebirges und besiegelten so auch das Ende der Baton'Fa. 
			</span>
  	</div>
	</div>
</div>
